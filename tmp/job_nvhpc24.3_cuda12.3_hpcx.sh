#!/bin/bash
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=4
#SBATCH --gres=gpu:4
#SBATCH --cpus-per-task=8
#SBATCH --mem=490000
#SBATCH --account=EUHPC_E01_022
#SBATCH --partition=boost_usr_prod
#SBATCH --qos=boost_qos_dbg
#SBATCH --time=0:30:00
#SBATCH --error=err.job-%j
#SBATCH --output=out.job-%j

#module load spack/0.21.0-68a 
#module load netlib-scalapack/2.2.0--openmpi--4.1.6--nvhpc--24.3-uw4suuz
#module load devicexlib/develop--nvhpc--24.3-cuda-12.3-cuf
#module load /leonardo/prod/spack/5.2/install/0.21/linux-rhel8-icelake/gcc-8.5.0/nvhpc-24.3-v63z4inohb4ywjeggzhlhiuvuoejr2le/modulefiles/nvhpc-hpcx-cuda12/24.3
#module load /leonardo_scratch/large/userexternal/mbettenc/compiler/modulefiles/nvhpc-hpcx-cuda12/24.3
module load /leonardo_work/Max3_devel_2/nspallan/nvidia/hpc_sdk/modulefiles/nvhpc-hpcx-cuda12/24.3
DEVICEXLIB_HOME=/leonardo/pub/userexternal/nspallan/spack-0.21.0-5.2/install/linux-rhel8-icelake/nvhpc-24.3/devicexlib-develop-qnn4okvde3dpue6mptwyyo2vvil2ptif
DEVICEXLIB_LIB=$DEVICEXLIB_HOME/lib
DEVICEXLIB_INC=$DEVICEXLIB_HOME/include

#nvidia-smi

export LD_LIBRARY_PATH=$DEVICEXLIB_LIB:$LD_LIBRARY_PATH

export CUSOLVERMP_LOG_LEVEL=1
export CUSOLVERMP_LOG_MASK=1
export CUSOLVERMP_LOG_MASK="logfile_%i.log"

myprogram="cusolvermp_test.f90"
#myprogram="cusolvermp_test_nodevxlib.f90"

rm -f mp_test.x tmp.f90 fort.101 fort.102
#PPFLAGS="-D__CUDA -D__CUSOLVER_MP -D__BENCHMARK -D__DEBUG -D__RHS1"
PPFLAGS="-D__CUDA -D__CUSOLVER_MP"
nvfortran -Mpreprocess -E $PPFLAGS $myprogram > tmp.f90
mpif90 -o mp_test.x tmp.f90 \
       -cuda -gpu=cc80,cuda12.3 \
       -cudalib=cublas,cusolver,nccl,cusolvermp \
       -I$DEVICEXLIB_INC -L$DEVICEXLIB_LIB -ldevXlib -lscalapack -llapack -lblas 

mpirun ./mp_test.x
