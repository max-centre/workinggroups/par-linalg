program es_scalapack
use mpi
implicit none

integer :: i, j, stdout
parameter ( stdout=6 )
! MPI
integer :: ierr, nprocs, myrank
! BLACS/SCALAPACK
integer :: context, nprow, npcol, myrow, mycol, info, ndims, dims(2)
parameter ( ndims=2 )
external :: blacs_exit, blacs_gridexit, blacs_gridinfo, blacs_get
external :: blacs_gridinit, descinit, numroc, pdelset, pdlaprnt
integer :: numroc
! Matrix
integer :: M, N, MB, NB, Nloc, Mloc, aval
parameter ( M=16384, N=16384, MB=64, NB=64 )
!parameter ( M=10, N=10, MB=2, NB=2 )
real*8 , allocatable :: A(:,:), B(:), work(:)
integer, allocatable :: ipiv(:)
integer :: descA(9), descB(9)
! Benchmarks
real*8 :: t1, t2, dt

! Initialize MPI environment
call MPI_Init(ierr)
call MPI_Comm_Size(mpi_comm_world,nprocs,ierr)
call MPI_Comm_Rank(mpi_comm_world,myrank,ierr)

! Initialize a default BLACS context and the processes grid
dims = 0
call MPI_Dims_Create( nprocs, ndims, dims, ierr)
nprow = dims(1)
npcol = dims(2)
call blacs_get( -1, 0, context )
call blacs_gridinit( context, 'Row-major', nprow, npcol)
call blacs_gridinfo( context, nprow, npcol, myrow, mycol )

! Computation of local matrix size
Mloc = numroc( M, MB, myrow, 0, nprow )
Nloc = numroc( N, NB, mycol, 0, npcol )
write(*,*) "myrank= ",myrank,"     Mloc= ",Mloc,"     Nloc= ",Nloc 
allocate( A( Mloc, Nloc ) )
allocate( B( Mloc ) )
allocate( ipiv( Mloc+MB ) )
allocate( work( M ) )

! Descriptos
call descinit(descA, M, N, MB, NB, 0, 0, context, max(1,Mloc), info)
call descinit(descB, M, 1, MB,  1, 0, 0, context, max(1,Mloc), info)

! Vector and matrix elements generation
do j=1,N
  do i=1,M
    if(i.eq.j) then
      call pdelset( A, i, j, descA, dble(10000) )
   else
      call pdelset( A, i, j, descA, (dble(i)+dble(j)/dble(2.0)) )
   endif
  enddo
enddo
do i=1,M
  call pdelset( B, i, 1, descB, dble(207-i) )
enddo

!!! for benchmarking !!!
call MPI_Barrier( mpi_comm_world, ierr)
t1 = MPI_wtime()
!!!!!!!!!!!!!!!!!!!!!!!!

! Linear system equations solver
call pdgesv( N, 1, A, 1, 1, descA, ipiv, B, 1, 1, descB, info )

!!! for benchmarking !!!
call MPI_Barrier( mpi_comm_world, ierr)
t2 = MPI_wtime()
dt = t2-t1
if(myrank.eq.0) write(*,*) nprocs, dt
!!!!!!!!!!!!!!!!!!!!!!!!

! Print the results vector
call pdlaprnt(M, 1, B, 1, 1, descB, 0, 0, 'X', stdout, work)

deallocate( A )
deallocate( B )
deallocate( ipiv )
deallocate( work )

! Close BLACS environment
call blacs_gridexit( context )
call blacs_exit( 0 )

! Close MPI environment if blacs_exit paramater is not equal zero
!call MPI_Finalize(ierr)

end program es_scalapack  

