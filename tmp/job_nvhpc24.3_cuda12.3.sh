#!/bin/bash
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=4
#SBATCH --gres=gpu:4
#SBATCH --cpus-per-task=8
#SBATCH --mem=490000
#SBATCH --account=EUHPC_E01_022
#SBATCH --partition=boost_usr_prod
#SBATCH --qos=boost_qos_dbg
#SBATCH --time=0:30:00
#SBATCH --error=err.job-%j
#SBATCH --output=out.job-%j

module load spack/0.21.0-68a 
module load netlib-scalapack/2.2.0--openmpi--4.1.6--nvhpc--24.3-uw4suuz
module load devicexlib/develop--nvhpc--24.3-cuda-12.3-cuf
#module load cuda/12.3
export MYCUDA_HOME=$NVHPC_HOME/Linux_x86_64/24.3/cuda/12.3
export LD_LIBRARY_PATH=$MYCUDA_HOME/compat:$MYCUDA_HOME/targets/x86_64-linux/lib:$LD_LIBRARY_PATH

#echo $CUDA_VISIBLE_DEVICES
#nvidia-smi

#echo $LD_LIBRARY_PATH

#mpif90 -o mp_test.x cusolvermp_test.f90 -cuda -gpu=cc80,cuda12.3 -cudalib=cusolvermp -I$DEVICEXLIB_INC -L$DEVICEXLIB_LIB -ldevXlib -L$NETLIB_SCALAPACK_LIB -lscalapack -L$OPENBLAS_LIB -lopenblas -rpath $MYCUDA_HOME/compat

srun ./mp_test.x
