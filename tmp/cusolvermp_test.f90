program main
  use mpi
#ifdef __CUDA
  use cudafor
  use cusolverdn
#ifdef __CUSOLVER_MP
  use cusolvermp
#endif
  use devxlib, only: devxlib_alloc, devxlib_allocated_p, devxlib_free,&
                     devxlib_memcpy_h2d_p, devxlib_memcpy_d2h_p
#endif
  use iso_fortran_env, only : int32, int64, real32, real64, output_unit, error_unit
  use iso_c_binding
implicit none

interface
  integer( kind = C_INT ) function gethostname( hname, len ) bind( C, name = 'gethostname' )
      use iso_c_binding
      implicit none
      character( kind = C_CHAR ) :: hname( * )
      integer( kind = C_INT ), VALUE :: len
  end function gethostname
end interface

integer :: i, j, stdout
character(20)  :: subname="MAIN PROGRAM"
character(20)  :: hostname
integer( kind = C_INT ), parameter :: sl = 100
integer, parameter :: DXL_OK = 0

! MPI
integer :: ierr, nprocs, myrank, tmp_comm

! CUDA
#ifdef __CUDA
integer(kind=cuda_stream_kind) :: mystream
#ifdef __CUSOLVER_MP
type(cal_comm) :: comm_h
type(cusolverMpHandle) :: cusolvermp_h
type(cusolverMpGrid) :: grid
type(cusolverMpMatrixDescriptor)  :: d_descA, d_descB
#endif
integer, device :: info_d
#endif
integer :: local_device

! BLACS/SCALAPACK
integer :: context, nprow, npcol, myrow, mycol, info, ndims, dims(2)
parameter ( ndims=2 )
external :: blacs_exit, blacs_gridexit, blacs_gridinfo, blacs_get
external :: blacs_gridinit, descinit, numroc, pdelset, pdlaprnt
integer :: numroc

! Matrix
integer(int64) :: M, N, MB, NB, Nloc, Mloc, NRHS, aval, uno, zero
integer(int64) :: d_Mloc, d_Nloc
!parameter ( M=16384, N=16384, MB=64, NB=64, uno=1, zero=0 )
parameter ( M=1024, N=1024, MB=16, NB=16, uno=1, zero=0 )
real(real64), pointer, contiguous :: A(:,:) => null()
real(real64), pointer, contiguous, device :: A_d(:,:) => null()
integer, pointer, contiguous :: ipiv(:)  => null()
integer(int64), pointer, contiguous, device :: ipiv_d(:)  => null()
#ifdef __RHS1
real(real64), pointer, contiguous :: B(:) => null()
real(real64), pointer, contiguous :: X(:) => null()
real(real64), pointer, contiguous, device :: B_d(:) => null()
#else
real(real64), pointer, contiguous :: B(:,:) => null()
real(real64), pointer, contiguous :: X(:,:) => null()
real(real64), pointer, contiguous, device :: B_d(:,:) => null()
#endif
real(real64), pointer, contiguous :: work_trf(:) => null()
real(real64), pointer, contiguous :: work_trs(:) => null()
real(real64), pointer, contiguous, device :: work_trf_d(:) => null()
real(real64), pointer, contiguous, device :: work_trs_d(:) => null()
integer :: descA(9), descB(9)
integer(int64) :: d_bytes_trf, h_bytes_trf, d_bytes_trs, h_bytes_trs
real(real64), allocatable :: work(:)

! Benchmarks
real(real64) :: t1, t2, dt, tol=0.001

#ifdef __RHS1
NRHS = 1
#else
NRHS = N
#endif

! Initialize MPI environment
call MPI_Init(ierr)
call MPI_Comm_Size(mpi_comm_world,nprocs,ierr)
call MPI_Comm_Rank(mpi_comm_world,myrank,ierr)

call MPI_COMM_SPLIT_TYPE(mpi_comm_world, MPI_COMM_TYPE_SHARED, 0, MPI_INFO_NULL, tmp_comm, ierr)
call MPI_Comm_Rank(tmp_comm,local_device,ierr)
call MPI_Comm_Free(tmp_comm,ierr)

#ifdef __CUSOLVER_MP
! Creating communicator handle with MPI communicator
ierr = cal_comm_create_mpi(mpi_comm_world, myrank, nprocs, local_device, comm_h)
if(ierr/=CAL_OK) call errore(subname,'cal_comm_create_mpi failed', ierr)
#endif
#ifdef __DEBUG
ierr = gethostname(hostname, sl)
write(*,*) trim(hostname), 'rank:', myrank, 'local_device:', local_device
#endif
#ifdef __CUDA
ierr = cudaSetDevice(local_device)
if(ierr/=CUDASUCCESS) call errore(subname,'cudaSetDevice failed', ierr)
! Create local stream
ierr = cudaStreamCreate(mystream)
if(ierr/=CUDASUCCESS) call errore(subname,'cudaStreamCreate failed', ierr)
!mystream = cudaforGetDefaultStream()
#endif

! Initialize a default BLACS context and the processes grid
dims = 0
call MPI_Dims_Create( nprocs, ndims, dims, ierr)
nprow = dims(1)
npcol = dims(2)
call blacs_get( -1, 0, context )
call blacs_gridinit( context, 'Row-major', nprow, npcol)
call blacs_gridinfo( context, nprow, npcol, myrow, mycol )

#ifdef __CUSOLVER_MP
! Initialize the cuSOLVER environment
ierr = cusolverMpCreate(cusolvermp_h, local_device, mystream)
if(ierr/=CUSOLVER_STATUS_SUCCESS) call errore(subname,'cusolverMpCreate failed', ierr)
ierr = cusolverMpCreateDeviceGrid(cusolvermp_h, grid, comm_h, nprow, npcol, CUSOLVERMP_GRID_MAPPING_COL_MAJOR)
if(ierr/=CUSOLVER_STATUS_SUCCESS) call errore(subname,'cusolverMpCreateDeviceGrid failed', ierr)
#endif

! Computation of local matrix size
Mloc = numroc( M, MB, myrow, 0, nprow )
Nloc = numroc( N, NB, mycol, 0, npcol )
#ifdef __DEBUG
write(*,*) "myrank:", myrank, "Mloc:", Mloc, "Nloc:", Nloc 
#endif
allocate( A( Mloc, Nloc ) )
#ifdef __RHS1
allocate( B( Mloc ) )
#else
allocate( B( Mloc, Nloc ) )
#endif
allocate( ipiv( Mloc+MB ) )
#ifdef __CUSOLVER_MP
d_Mloc = cusolverMpNumROC(M, MB, myrow, 0, nprow)
d_Nloc = cusolverMpNumROC(N, NB, mycol, 0, npcol)
if(d_Mloc/=Mloc .or. d_Nloc/=Nloc) call errore(subname,'cusolverMpNumROC - rank:', myrank)
#ifdef __DEBUG
write(*,*) "myrank:", myrank, "d_Mloc:", d_Mloc, "d_Nloc:", d_Nloc 
#endif
#endif
#ifdef __CUDA
call devxlib_alloc(A_d, (/d_Mloc,d_Nloc/), ierr)
if(ierr/=DXL_OK) call errore(subname,'devxlib_alloc failed: A_d', ierr)
#ifdef __RHS1
call devxlib_alloc(B_d, d_Mloc, ierr)
if(ierr/=DXL_OK) call errore(subname,'devxlib_alloc failed: B1_d', ierr)
#else
call devxlib_alloc(B_d, (/d_Mloc,d_Nloc/), ierr)
if(ierr/=DXL_OK) call errore(subname,'devxlib_alloc failed: B_d', ierr)
#endif
call devxlib_alloc(ipiv_d, d_Mloc+MB, ierr)
if(ierr/=DXL_OK) call errore(subname,'devxlib_alloc failed: ipiv_d', ierr)
#endif

! Descriptos
call descinit(descA, M, N, MB, NB, 0, 0, context, max(1,Mloc), info)
call descinit(descB, M, NRHS, MB, min(NRHS,NB), 0, 0, context, max(1,Mloc), info)
#ifdef __CUSOLVER_MP
ierr = cusolverMpCreateMatrixDesc(d_descA, grid, CUDADATATYPE(CUDA_R_64F), M, N, MB, NB, 0, 0, max(1,d_Mloc))
if(ierr/=CUSOLVER_STATUS_SUCCESS) call errore(subname,'cusolverMpCreateMatrixDesc failed: d_descA', ierr)
ierr = cusolverMpCreateMatrixDesc(d_descB, grid, CUDADATATYPE(CUDA_R_64F), M, NRHS, MB, min(NRHS,NB), 0, 0, max(1,d_Mloc))
if(ierr/=CUSOLVER_STATUS_SUCCESS) call errore(subname,'cusolverMpCreateMatrixDesc failed: d_descB', ierr)
#endif

! Vector and matrix elements generation
do j=1,N
  do i=1,M
    if(i.eq.j) then
      call pdelset( A, i, j, descA, real(10000.0, real64) )
   else
      call pdelset( A, i, j, descA, (real(i,real64)+real(j,real64)/real(2.0,real64)) )
   endif
  enddo
enddo
B = real(0.0,real64)
do j=1,NRHS
  call pdelset( B, j, j, descB, real(1.0,real64) )
enddo

#ifdef __CUDA
! Copy matrices on device
call devxlib_memcpy_h2d_p(A_d, A)
if(ierr/=DXL_OK) call errore(subname,'devxlib_memcpy_h2d_p failed: A_d=A', ierr)
call devxlib_memcpy_h2d_p(B_d, B)
if(ierr/=DXL_OK) call errore(subname,'devxlib_memcpy_h2d_p failed: B_d=B', ierr)
#endif

! Stream synchronization needed before call cusolver buffer functions
#if defined(__CUSOLVER_MP)
ierr = cal_stream_sync(comm_h, mystream)
if(ierr/=CAL_OK) call errore(subname,'cal_stream_sync failed', ierr)
#elif defined(__CUDA)
ierr = cudaStreamSynchronize( mystream )
if(ierr/=CUDASUCCESS) call errore(subname,'cudaStreamSynchronize failed', ierr)
#endif

#ifdef __CUSOLVER_MP
! Query and allocating workspaces
ierr = cusolverMpGetrf_buffersize(cusolvermp_h, &
    M, N, A_d, uno, uno, d_descA, ipiv_d, CUDADATATYPE(CUDA_R_64F), &
    d_bytes_trf, h_bytes_trf)
if(ierr/=CUSOLVER_STATUS_SUCCESS) call errore(subname,'cusolverMpGetrf_buffersize failed', ierr)
ierr = cusolverMpGetrs_buffersize(cusolvermp_h, CUBLAS_OP_N, &
    M, NRHS, A_d, uno, uno, d_descA, ipiv_d, B_d, uno, uno, d_descB, CUDADATATYPE(CUDA_R_64F), &
    d_bytes_trs, h_bytes_trs)
if(ierr/=CUSOLVER_STATUS_SUCCESS) call errore(subname,'cusolverMpGetrs_buffersize failed', ierr)
#ifdef __DEBUG
write(*,*) 'TRF rank:', myrank, 'd:', d_bytes_trf, 'h:', h_bytes_trf
write(*,*) 'TRS rank:', myrank, 'd:', d_bytes_trs, 'h:', h_bytes_trs
#endif
allocate( work_trf ( h_bytes_trf/c_sizeof(tol) ) )
call devxlib_alloc(work_trf_d, d_bytes_trf/c_sizeof(tol), ierr)
if(ierr/=DXL_OK) call errore(subname,'devxlib_alloc failed: work_trf_d(d_bytes_trf/8)', ierr)
allocate( work_trs ( h_bytes_trs/c_sizeof(tol) ) )
call devxlib_alloc(work_trs_d, d_bytes_trs/c_sizeof(tol), ierr)
if(ierr/=DXL_OK) call errore(subname,'devxlib_alloc failed: work_trf_s(d_bytes_trs/8)', ierr)
#endif

#ifdef __BENCHMARK
! Benchmark
call MPI_Barrier( mpi_comm_world, ierr)
t1 = MPI_wtime()
#endif

call pdgetrf( M, N, A, 1, 1, descA, ipiv, ierr )
call pdgetrs( 'N', M, NRHS, A, 1, 1, descA, ipiv, B, 1, 1, descB, ierr )

#ifdef __BENCHMARK
! Benchmark
call MPI_Barrier( mpi_comm_world, ierr)
t2 = MPI_wtime()
dt = t2-t1
if(myrank.eq.0) write(*,*) "ScaLAPACK => nprocs:", nprocs, "dt:", dt
#endif

#ifdef __DEBUG
! Print the ScaLAPACK results vector
allocate( work( MB ) )
call pdlaprnt(M, NRHS, B, 1, 1, descB, 0, 0, 'Ainv', 101, work)
#endif

#ifdef __CUSOLVER_MP
ierr = cal_stream_sync(comm_h, mystream)
if(ierr/=CAL_OK) call errore(subname,'cal_stream_sync failed', ierr)
#ifdef __BENCHMARK
! Benchmark
call MPI_Barrier( mpi_comm_world, ierr)
t1 = MPI_wtime()
#endif

ierr = cusolverMpGetrf(cusolvermp_h, &
     M, N, A_d, uno, uno, d_descA, ipiv_d, CUDADATATYPE(CUDA_R_64F), &
     work_trf_d, d_bytes_trf, work_trf, h_bytes_trf, info)
if(ierr/=CUSOLVER_STATUS_SUCCESS) call errore(subname,'cusolverMpGetrf failed', ierr)

ierr = cal_stream_sync(comm_h, mystream)
if(ierr/=CAL_OK) call errore(subname,'cal_stream_sync failed', ierr)

ierr = cusolverMpGetrs(cusolvermp_h, CUBLAS_OP_N, &
     M, NRHS, A_d, uno, uno, d_descA, ipiv_d, B_d, uno, uno, d_descB, &
     CUDADATATYPE(CUDA_R_64F), &
     work_trs_d, d_bytes_trs, work_trs, h_bytes_trs, info_d)
if(ierr/=CUSOLVER_STATUS_SUCCESS) call errore(subname,'cusolverMpGetrs failed', ierr)

#ifdef __BENCHMARK
! Benchmark
call MPI_Barrier( mpi_comm_world, ierr)
t2 = MPI_wtime()
dt = t2-t1
if(myrank.eq.0) write(*,*) "nprocs:", nprocs, "dt:", dt
#endif

#ifdef __RHS1
allocate( X( Mloc ) )
#else
allocate( X( Mloc, Nloc ) )
#endif
call devxlib_memcpy_d2h_p(X, B_d)
if(ierr/=DXL_OK) call errore(subname,'devxlib_memcpy_d2h_p failed: X = B_d', ierr)
#ifdef __DEBUG
! Print the cusolverMp results vector
call pdlaprnt(M, NRHS, X, 1, 1, descB, 0, 0, 'Ainv', 102, work)
#endif

! Final check
do j=1,min(NRHS,Nloc)
   do i=1,Mloc
#ifdef __RHS1
      if (abs(B(i)-X(i)) >= tol) call errore(subname,'Final check failed', myrank)
#else
      if (abs(B(i,j)-X(i,j)) >= tol) call errore(subname,'Final check failed', myrank)
#endif
   enddo
enddo
#endif

! Deallocations
deallocate( A )
deallocate( B )
deallocate( ipiv )
#ifdef __DEBUG
deallocate( work )
#endif
#ifdef __CUSOLVER_MP
call devxlib_free(A_d, ierr)
if(ierr/=DXL_OK) call errore(subname,'devxlib_free failed: A_d', ierr)
call devxlib_free(B_d, ierr)
if(ierr/=DXL_OK) call errore(subname,'devxlib_free failed: B_d', ierr)
call devxlib_free(ipiv_d, ierr)
if(ierr/=DXL_OK) call errore(subname,'devxlib_free failed: ipiv_d', ierr)
deallocate( work_trf )
deallocate( work_trs )
call devxlib_free(work_trf_d, ierr)
if(ierr/=DXL_OK) call errore(subname,'devxlib_free failed: work_trf_d', ierr)
call devxlib_free(work_trs_d, ierr)
if(ierr/=DXL_OK) call errore(subname,'devxlib_free failed: work_trs_d', ierr)
deallocate(X)

! Close cuSOLVER environment
ierr = cusolverMpDestroyMatrixDesc(d_descA)
if(ierr/=CUSOLVER_STATUS_SUCCESS) call errore(subname,'cusolverMpDestroyMatrixDesc failed', ierr)
ierr = cusolverMpDestroyMatrixDesc(d_descB)
if(ierr/=CUSOLVER_STATUS_SUCCESS) call errore(subname,'cusolverMpDestroyMatrixDesc failed', ierr)
ierr = cusolverMpDestroyGrid(grid)
if(ierr/=CUSOLVER_STATUS_SUCCESS) call errore(subname,'cusolverMpDestroyGrid failed', ierr)
ierr = cusolverMpDestroy(cusolvermp_h)
if(ierr/=CUSOLVER_STATUS_SUCCESS) call errore(subname,'cusolverMpDestroy failed', ierr)

! Destroying communicator handle
ierr = cal_comm_destroy(comm_h)
if(ierr/=CAL_OK) call errore(subname,'cal_comm_destroy failed', ierr)
#endif

! Close BLACS environment
call blacs_gridexit( context )
call blacs_exit( 0 )

! Close MPI environment if blacs_exit paramater is not equal zero
!call MPI_Finalize(ierr)

contains
  
  SUBROUTINE errore( calling_routine, message, ierr )
    use mpi
    use iso_fortran_env, only : output_unit, error_unit
    IMPLICIT NONE
    !
    INTEGER,       PARAMETER :: crashunit = 50
    CHARACTER(10), PARAMETER :: crash_file = "CRASH"
    !
    CHARACTER(LEN=*), INTENT(IN) :: calling_routine, message
      ! the name of the calling calling_routinee
      ! the output messagee
    INTEGER,          INTENT(IN) :: ierr
      ! the error flag
    INTEGER                      :: mpierr
      ! the task id  
    !
    IF ( ierr <= 0 ) RETURN
    !
    ! ... the error message is written in the error_unit
    !
    !WRITE( UNIT = error_unit, FMT = '(/,1X,78("%"))' )
    WRITE( UNIT = error_unit, FMT = '(5X,"from ",A)' ) TRIM(calling_routine)
    WRITE( UNIT = error_unit, FMT = '(5X,A," : error #",I10)' ) message, ierr
    !WRITE( UNIT = error_unit, FMT = '(1X,78("%"),/)' )
    WRITE( UNIT = error_unit, FMT = '("     stopping ...")' )
    !
    !CALL MPI_ABORT( MPI_COMM_WORLD, mpierr )
    CALL MPI_FINALIZE( mpierr )
    !
    STOP 2
    !
    RETURN
    !
  END SUBROUTINE errore

end program main

