# par-linalg

## How to compile the code

Copy in the main directory one of the files in config/make.sys_* proper for your purpose and name it make.sys.
Set in the file the right path for the scalapack and cusolverMg libraries.

Then compile with
```
make all
```

## How to launch the code

Here the program options:
```
    "-n", "--ndim"
        Dimension of the square matrix (default 500, that mean 500x500).
	
    "-nr", "-nrow"
        Number of rows of the processing grid (default 1).
	
    "-nc", "-ncol"
        Number of columns of the processing grid (default 1).
	
    "-np", "--npool"
        Number of parallel pools (default 1).
	
    "-nb", "--ndim_block"
        Dimension of the scalapack blocks (nbxnb) (default 0, the dimension is the biggest possible).
	
    "-s", "--serial"
        1 to run the serial algorithm, 0 to skip it (default 0).
	
    "-p", "--parallel"
        1 to run the parallel (scalapack) algorithm, 0 to skip it (default 1).
	
    "-sg", "--serial_gpu"
        1 to run the GPU serial (cusolver) algorithm, 0 to skip it (default 1).
	
    "-pg", "--parallel_gpu"
        1 to run the GPU parallel (cusolverMg) algorithm, 0 to skip it (default 1).
```

### Example:

```
# if needed set this variable
export CUDA_VISIBLE_DEVICES=0,1,2,3

srun -n ${SLURM_NTASKS} \
     mytest.x -n 4096 -nrow 1 -ncol 4 -np 1 -nb 256 > mytest.out.${SLURM_JOBID}
```
