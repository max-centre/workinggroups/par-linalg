#
#mpif90 -C -g example1.f -o example1.x -lmkl_intel_lp64 -lmkl_sequential -lmkl_core -lmkl_scalapack_lp64 -lmkl_blacs_lp64
#
module purge
module load profile/intel
module load scalapack 
#
mpif90 -C -g example1.f -o example1.x -lscalapack -lmkl_intel_lp64 -lmkl_sequential -lmkl_core
#
