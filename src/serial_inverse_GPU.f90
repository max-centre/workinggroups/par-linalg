
#ifdef __CUDA
!===========================================
subroutine serial_inverse_GPU( ndim, Amat_d, Ainv_d )
  !===========================================
  use kinds
  use util_module
  use para_module
  use cuda_m
  implicit none
  !
  integer   :: ndim
  real(dbl),device :: Amat_d(ndim,ndim)
  real(dbl),device :: Ainv_d(ndim,ndim)
  !
  real(dbl),device,allocatable :: Awork_d(:,:),v_real_d(:)
  real(dbl),       allocatable :: id(:,:)        
  integer,  device,allocatable :: v_int_d(:),vp_int_d(:)
  integer   :: ierr, lwork, i
  character(20) :: subname='serial_inverse_GPU'
  !
  !
  if(.not.cuda_linalg_init) call cuda_linalg_setup()
  !
  allocate(Awork_d(ndim,ndim))
  allocate(id(ndim,ndim))
  allocate(v_int_d(ndim))
  allocate(vp_int_d(ndim))
  !
  id = 0.0
  do i=1,ndim
    id(i,i) = 1.0
  enddo
  !
  Awork_d = Amat_d
  Ainv_d = id
  !
  ierr = cusolverDnDgetrf_bufferSize(cusolv_h,ndim,ndim,Awork_d,ndim,lwork)
  if(ierr/=CUSOLVER_STATUS_SUCCESS) call errore(subname,'cusolverDnDgetrf_bufferSize failed',10)
  !
  allocate(v_real_d(lwork))
  !
  ierr = cusolverDnDgetrf(cusolv_h,ndim,ndim,Awork_d,ndim,v_real_d,v_int_d,vp_int_d)
  if(ierr/=CUSOLVER_STATUS_SUCCESS) call errore(subname,'cusolverDnDgetrf failed',10)
  !
  ierr = cusolverDnDgetrs(cusolv_h,CUBLAS_OP_N,ndim,ndim,Awork_d,ndim,v_int_d,Ainv_d,ndim,vp_int_d)
  if(ierr/=CUSOLVER_STATUS_SUCCESS) call errore(subname,'cusolverDnDgetrs failed',10)
  !
  if (ierr/=0 ) call errore('serial_inverse','inverting Amat',10)
  !
  !
  deallocate(Awork_d)
  deallocate(id)
  deallocate(v_int_d)
  deallocate(vp_int_d)
  !
#ifdef __MPI
  call MPI_barrier( mpi_comm_world, ierr)
#endif
  !
  return
  !
end subroutine serial_inverse_GPU
!
#endif

subroutine serial_inverse_GPU__dummy()
  write(*,*)
end subroutine 

