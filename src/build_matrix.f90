
!===========================================
  subroutine build_matrix( ndim, Amat )
  !===========================================
  !
  ! build A = I + scal * randmat
  ! a small value of scal ensures A is invertible
  !
  use kinds
  use util_module
  use para_module
#ifdef __INTEL
  use ifport
#endif
  implicit none
  !
  integer   :: ndim
  real(dbl) :: Amat(ndim,ndim)
  !
  real(dbl) :: scal
  real(dbl) :: diag_val
  !
  integer   :: i, j, ierr
  real(dbl), allocatable :: w(:), zmat(:,:)
  
  !
  ! setting parameters
  !
  !scal=0.2d0        ! 0.2
  !diag_val=2.0d0    ! 1.0
  scal=1.0d0
  diag_val=real(ndim)*scal
  !
  if ( me_pool == root_pool ) then
     !
     call random_number(Amat)
     !
     do j = 1, ndim
     do i = j, ndim
         Amat(i,j) = Amat(i,j) * scal
         Amat(j,i) = Amat(i,j)
     enddo
     enddo
     !
     do i = 1, ndim
      Amat(i,i) = Amat(i,i) + diag_val
     enddo
     !
     write(ndim,"(f15.9)") Amat
  endif
  !
#ifdef __MPI
  call MPI_bcast(Amat,ndim*ndim,MPI_DOUBLE_PRECISION,root_pool,intra_pool_comm,ierr)
#endif 
  !
  ! compute and report eigenvalues
  !
  allocate( w(ndim), zmat(ndim,ndim) )
  call mat_hdiag( zmat, w, Amat, ndim )
  !
  if ( me_pool == root_pool ) then 
     write(*,"(5x,i4,' Amat Eigenvalues')") my_pool_id
     !   
     ! print the 8 largest eigs
     write(*,"(5x,i4,100f15.9)") my_pool_id, w(ndim:max(1,ndim-7):-1)
     ! print the 8 smallest  eigs
     write(*,"(5x,i4,100f15.9)") my_pool_id, w(1:min(ndim,8))
  endif
  !
  deallocate( w, zmat )
  !
#ifdef __MPI
  call MPI_barrier( mpi_comm_world, ierr)
#endif
  !
  return
  !
end subroutine build_matrix

