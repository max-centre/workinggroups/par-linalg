!
! Copyright (C) 2021, MaX CoE
! Distributed under the MIT License 
! (license terms are at http://opensource.org/licenses/MIT).
!
!--
!
! interface to timing routines and other utilities, 
! mostly used in tests
!
module dev_utils_m
  !
  use iso_c_binding
  use iso_fortran_env
  implicit none
  private
 
  complex(real32), parameter :: ci_sp=cmplx(0.0,1.0,kind=real32)
  complex(real64), parameter :: ci_dp=cmplx(0.0,1.0,kind=real64)

  interface 
    function wallclock() bind(C,name="wallclock_c")
       use iso_c_binding, only: c_double
       real(c_double) :: wallclock
    end function
  end interface

  interface random_number_cmplx
    module procedure :: random_number_cmplx_c1
    module procedure :: random_number_cmplx_c2
    module procedure :: random_number_cmplx_z1
    module procedure :: random_number_cmplx_z2
  end interface

  public :: wallclock
  public :: random_number_cmplx

contains

  subroutine random_number_cmplx_c1(array)
    implicit none
    complex(real32) :: array(:)
    real(real32), allocatable :: rtmp(:)
    !
    allocate(rtmp,mold=real(array))
    call random_number(rtmp)
    array=rtmp
    call random_number(rtmp)
    array=array+ci_sp*rtmp
    deallocate(rtmp)
  end subroutine

  subroutine random_number_cmplx_c2(array)
    implicit none
    complex(real32) :: array(:,:)
    real(real32), allocatable :: rtmp(:,:)
    !
    allocate(rtmp,mold=real(array))
    call random_number(rtmp)
    array=rtmp
    call random_number(rtmp)
    array=array+ci_sp*rtmp
    deallocate(rtmp)
  end subroutine

  subroutine random_number_cmplx_z1(array)
    implicit none
    complex(real64) :: array(:)
    real(real64), allocatable :: rtmp(:)
    !
    allocate(rtmp,mold=real(array))
    call random_number(rtmp)
    array=rtmp
    call random_number(rtmp)
    array=array+ci_sp*rtmp
    deallocate(rtmp)
  end subroutine

  subroutine random_number_cmplx_z2(array)
    implicit none
    complex(real64) :: array(:,:)
    real(real64), allocatable :: rtmp(:,:)
    !
    allocate(rtmp,mold=real(array))
    call random_number(rtmp)
    array=rtmp
    call random_number(rtmp)
    array=array+ci_sp*rtmp
    deallocate(rtmp)
  end subroutine

end module dev_utils_m

