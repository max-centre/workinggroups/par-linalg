
  !----------------------------------------------------------------------------
  SUBROUTINE init_pools( parent_comm )
    !---------------------------------------------------------------------------
    !
    use para_module
    IMPLICIT NONE
    !
    INTEGER, INTENT(IN) :: parent_comm
    !
    INTEGER :: ierr = 0
    INTEGER :: parent_nproc = 1
    INTEGER :: parent_mype  = 0
    !
#if defined (__MPI)
    !
    CALL MPI_COMM_SIZE(parent_comm,parent_nproc,ierr)
    CALL MPI_COMM_RANK(parent_comm,parent_mype,ierr)
    !
    ! ... number of cpus per pool of k-points (they are created inside each image)
    !
    nproc_pool = parent_nproc / npool
    !
    IF ( MOD( parent_nproc, npool ) /= 0 ) &
         CALL errore( 'init_pools', 'invalid number of pools, parent_nproc /= nproc_pool * npool', 1 )
    !
    ! ... my_pool_id  =  pool index for this processor    ( 0 : npool - 1 )
    ! ... me_pool     =  processor index within the pool  ( 0 : nproc_pool - 1 )
    !
    my_pool_id = parent_mype / nproc_pool
    me_pool    = MOD( parent_mype, nproc_pool )
    !
    CALL MPI_barrier( parent_comm, ierr )
    !
    ! ... the intra_pool_comm communicator is created
    !
    CALL MPI_COMM_SPLIT( parent_comm, my_pool_id, parent_mype, intra_pool_comm, ierr )
    !
    IF ( ierr /= 0 ) &
       CALL errore( 'init_pools', 'intra pool communicator initialization', ABS(ierr) )
    !
    CALL MPI_barrier( parent_comm, ierr )
    !
    ! ... the inter_pool_comm communicator is created
    !
    CALL MPI_COMM_SPLIT( parent_comm, me_pool, parent_mype, inter_pool_comm, ierr )
    !
    IF ( ierr /= 0 ) &
       CALL errore( 'init_pools', 'inter pool communicator initialization', ABS(ierr) )
    !
#endif
    !
    RETURN
  END SUBROUTINE init_pools

  !
  !----------------------------------------------------------------------------
  SUBROUTINE init_ortho( nproc_ortho_in, parent_comm )
    !---------------------------------------------------------------------------
    !
    ! ... Ortho group initialization
    !
    use para_module
    !
    IMPLICIT NONE
    !
    INTEGER, INTENT(IN) :: nproc_ortho_in! read from command-line, 0 if unset
    INTEGER, INTENT(IN) :: parent_comm   ! communicator of the parent group
    !
    INTEGER :: nproc_ortho_try
    INTEGER :: parent_nproc  ! nproc of the parent group
    INTEGER :: ierr = 0
    !

#if defined __SCALAPACK
    !parent_nproc = mp_size( parent_comm )
    CALL MPI_COMM_SIZE(parent_comm,parent_nproc,ierr)
    !
    ! define a 1D grid containing all MPI task of MPI_COMM_WORLD communicator
    !
    CALL BLACS_PINFO( me_blacs, np_blacs )
    CALL BLACS_GET( -1, 0, world_cntx )
    CALL BLACS_GRIDINIT( world_cntx, 'Row', 1, np_blacs )
    !
#endif
    !
    IF( nproc_ortho_in > 0 ) THEN
       ! command-line argument -ndiag N or -nproc N set to N
       ! use the command line value ensuring that it falls in the proper range
       nproc_ortho_try = MIN( nproc_ortho_in , parent_nproc )
    ELSE 
       ! no command-line argument -ndiag N or -nproc N is present
       ! insert here custom architecture specific default definitions
#if defined __SCALAPACK
       nproc_ortho_try = MAX( parent_nproc/2, 1 )
#else
       nproc_ortho_try = 1
#endif
    END IF
    !
    ! the ortho group for parallel linear algebra is a sub-group of the pool,
    ! then there are as many ortho groups as pools.
    !
    CALL init_ortho_group( nproc_ortho_try, parent_comm )
    !  
    RETURN
    !
  END SUBROUTINE init_ortho
  !
  !
  SUBROUTINE init_ortho_group( nproc_try_in, comm_all )
    !
    use para_module
    !
    IMPLICIT NONE
    !
    INTEGER, INTENT(IN) :: nproc_try_in, comm_all

    LOGICAL, SAVE :: first = .true.
    INTEGER :: ierr, color, key, me_all, nproc_all, nproc_try

#if defined __SCALAPACK
    INTEGER, ALLOCATABLE :: blacsmap(:,:), buff(:,:)
    INTEGER, ALLOCATABLE :: ortho_cntx_pe(:,:)
    INTEGER :: nprow, npcol, myrow, mycol, i, j
    INTEGER, EXTERNAL :: BLACS_PNUM
    !
    INTEGER :: nparent=1
    INTEGER :: total_nproc=1
    INTEGER :: total_mype=0
    INTEGER :: nproc_parent=1
    INTEGER :: my_parent_id=0
#endif


#if defined __MPI

    !me_all    = mp_rank( comm_all )
    !nproc_all = mp_size( comm_all )
    CALL MPI_COMM_RANK( comm_all, me_all, ierr )
    CALL MPI_COMM_SIZE( comm_all, nproc_all, ierr )
    !
    nproc_try = MIN( nproc_try_in, nproc_all )
    nproc_try = MAX( nproc_try, 1 )

    IF( .NOT. first ) THEN
       !  
       !  free resources associated to the communicator
       !
       !CALL mp_comm_free( ortho_comm )
       CALL MPI_comm_free( ortho_comm, ierr )
       !
#if defined __SCALAPACK
       IF(  ortho_comm_id > 0  ) THEN
          CALL BLACS_GRIDEXIT( ortho_cntx )
       ENDIF
       ortho_cntx = -1
#endif
       !
    END IF

    !  find the 1D column block closer (but lower) to nproc_try
    !
    CALL grid2d_dims( 'V', nproc_try, np_ortho(1), np_ortho(2) )
    !
    !  now, and only now, it is possible to define the number of tasks
    !  in the ortho group for parallel linear algebra
    !
    nproc_ortho = np_ortho(1) * np_ortho(2)
    !
    IF( nproc_all >= 4*nproc_ortho ) THEN
       !
       !  here we choose a processor every 4, in order not to stress memory BW
       !  on multi core procs, for which further performance enhancements are
       !  possible using OpenMP BLAS inside regter/cegter/rdiaghg/cdiaghg
       !  (to be implemented)
       !
       color = 0
       IF( me_all < 4*nproc_ortho .AND. MOD( me_all, 4 ) == 0 ) color = 1
       !
       leg_ortho = 4
       !
    ELSE IF( nproc_all >= 2*nproc_ortho ) THEN
       !
       !  here we choose a processor every 2, in order not to stress memory BW
       !
       color = 0
       IF( me_all < 2*nproc_ortho .AND. MOD( me_all, 2 ) == 0 ) color = 1
       !
       leg_ortho = 2
       !
    ELSE
       !
       !  here we choose the first processors
       !
       color = 0
       IF( me_all < nproc_ortho ) color = 1
       !
       leg_ortho = 1
       !
    END IF
    !
    key   = me_all
    !
    !  initialize the communicator for the new group by splitting the input communicator
    !
    CALL MPI_COMM_SPLIT( comm_all, color, key, ortho_comm, ierr )
    IF( ierr /= 0 ) &
         CALL errore( " init_ortho_group ", " initializing ortho group communicator ", ierr )
    !
    !  Computes coordinates of the processors, in row maior order
    !
    !me_ortho1   = mp_rank( ortho_comm )
    CALL MPI_COMM_RANK( ortho_comm, me_ortho1, ierr)
    !
    IF( me_all == 0 .AND. me_ortho1 /= 0 ) &
         CALL errore( " init_ortho_group ", " wrong root task in ortho group ", ierr )
    !
    if( color == 1 ) then
       ortho_comm_id = 1
       CALL GRID2D_COORDS( 'R', me_ortho1, np_ortho(1), np_ortho(2), me_ortho(1), me_ortho(2) )
       CALL GRID2D_RANK( 'R', np_ortho(1), np_ortho(2), me_ortho(1), me_ortho(2), ierr )
       IF( ierr /= me_ortho1 ) &
            CALL errore( " init_ortho_group ", " wrong task coordinates in ortho group ", ierr )
       IF( me_ortho1*leg_ortho /= me_all ) &
            CALL errore( " init_ortho_group ", " wrong rank assignment in ortho group ", ierr )

       CALL MPI_COMM_SPLIT( ortho_comm, me_ortho(2), me_ortho(1), ortho_col_comm, ierr )
       CALL MPI_COMM_SPLIT( ortho_comm, me_ortho(1), me_ortho(2), ortho_row_comm, ierr )

    else
       ortho_comm_id = 0
       me_ortho(1) = me_ortho1
       me_ortho(2) = me_ortho1
    endif
#if defined __SCALAPACK
    !
    !  This part is used to eliminate the image dependency from ortho groups
    !  SCALAPACK is now independent of whatever level of parallelization
    !  is present on top of pool parallelization
    !
    !total_nproc = mp_size(mpi_comm_world)
    !total_mype = mp_rank(mpi_comm_world)
    CALL MPI_COMM_SIZE(mpi_comm_world,total_nproc,ierr)
    CALL MPI_COMM_RANK(mpi_comm_world,total_mype,ierr)
    !
    nparent = total_nproc/npool/nproc_pool
    nproc_parent = total_nproc/nparent
    my_parent_id = total_mype/nproc_parent
    !
    ALLOCATE( ortho_cntx_pe( npool, nparent ) )
    ALLOCATE( blacsmap( np_ortho(1), np_ortho(2) ) )

    DO j = 1, nparent

       DO i = 1, npool

         CALL BLACS_GET( -1, 0, ortho_cntx_pe( i, j ) ) ! take a default value 

         blacsmap = 0
         nprow = np_ortho(1)
         npcol = np_ortho(2)

         IF( ( j == ( my_parent_id + 1 ) ) .and. &
             ( i == ( my_pool_id  + 1 ) ) .and. ( ortho_comm_id > 0 ) ) THEN

           blacsmap( me_ortho(1) + 1, me_ortho(2) + 1 ) = BLACS_PNUM( world_cntx, 0, me_blacs )

         END IF

         ! All MPI tasks defined in world comm take part in the definition of the BLACS grid
         !
         allocate( buff(size(blacsmap,1),size(blacsmap,2) ) )
         buff=blacsmap
         call MPI_ALLREDUCE( buff, blacsmap, size(blacsmap), MPI_INTEGER, MPI_SUM, MPI_COMM_WORLD, ierr)
         deallocate( buff )

         CALL BLACS_GRIDMAP( ortho_cntx_pe(i,j), blacsmap, nprow, nprow, npcol )

         CALL BLACS_GRIDINFO( ortho_cntx_pe(i,j), nprow, npcol, myrow, mycol )

         IF( ( j == ( my_parent_id + 1 ) ) .and. &
             ( i == ( my_pool_id  + 1 ) ) .and. ( ortho_comm_id > 0 ) ) THEN
            IF(  np_ortho(1) /= nprow ) &
               CALL errore( ' init_ortho_group ', ' problem with SCALAPACK, wrong no. of task rows ', 1 )
            IF(  np_ortho(2) /= npcol ) &
               CALL errore( ' init_ortho_group ', ' problem with SCALAPACK, wrong no. of task columns ', 1 )
            IF(  me_ortho(1) /= myrow ) &
               CALL errore( ' init_ortho_group ', ' problem with SCALAPACK, wrong task row ID ', 1 )
            IF(  me_ortho(2) /= mycol ) &
               CALL errore( ' init_ortho_group ', ' problem with SCALAPACK, wrong task columns ID ', 1 )

            ortho_cntx = ortho_cntx_pe(i,j)
         END IF

       END DO

    END DO 

    DEALLOCATE( blacsmap )
    DEALLOCATE( ortho_cntx_pe )


#endif

#else

    ortho_comm_id = 1

#endif

    first = .false.

    RETURN
  END SUBROUTINE init_ortho_group
  !
