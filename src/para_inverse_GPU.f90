
!===========================================
  subroutine para_inverse_GPU( ndim, Amat, Ainv, nb )
  !===========================================
  !
  ! perform the inversion by using scalapack
  !
  use kinds
  use util_module
  use para_module
  use cuda_m
  use iso_c_binding
  implicit none
  !
  integer, parameter :: dlen_ = 9
  !
  integer   :: ndim, nb
  real(dbl) :: Amat(ndim,ndim)
  real(dbl) :: Ainv(ndim,ndim)
  !
  integer   :: info, ierr
  !
#ifdef __CUSOLVER_MG
  !
  type DistData_t
    real(dbl), allocatable, device :: Amat(:,:)
    real(dbl), allocatable, device :: Ainv(:,:)
    integer,   allocatable, device :: ipiv(:)
    real(dbl), allocatable, device :: work(:)
  end type
  !
  character(20)  :: subname="para_inverse_GPU"
  character(256) :: cuda_visible_devices 
  integer   :: ndev
  !
  type(cusolverMgMatrixDesc_t) :: descA, descAinv
  type(cusolverMgGrid_t)       :: desc_grid
  integer(c_int64_t)           :: lwork, lwork_tmp
  integer   :: nprow, npcol, mycol
  integer   :: ndim_blc, lld, nblocks
  integer   :: i, id, ib
  integer   :: ils, ile, jls, jle, jgs, jge
  type(c_devptr),      allocatable :: Amat_p(:), Ainv_p(:), ipiv_p(:)
  type(c_devptr),      allocatable :: work_p(:)
  type(DistData_t),    allocatable :: data_d(:)
  integer,   allocatable :: deviceId(:)
  real(dbl), allocatable :: w(:), zmat(:,:)

  !
  ! spectator tasks
  ! only the master task of each pool does stuff
  if ( me_pool /= 0 ) return

  !
  ! get how many GPUs and initialize cusolverMg
  !
  call get_environment_variable("CUDA_VISIBLE_DEVICES",cuda_visible_devices)
  if (ionode) write(*,"(3x,'CUDA_VISIBLE_DEVICES: ',a)") trim(cuda_visible_devices)
  !
  ierr = cudaGetDeviceCount( ndev )
  !
  nprow=1
  npcol=ndev
  !
  allocate(deviceId(ndev))
  do id=1,ndev
    deviceId(id)=id-1
  enddo
  !
  ierr = cusolverMgDeviceSelect(cusolvmg_h,ndev,deviceId)
  if(ierr/=CUSOLVER_STATUS_SUCCESS) call errore(subname,'cusolverMgDeviceSelect failed',10)
  !
  ierr = cusolverMgCreateDeviceGrid(desc_grid,int(nprow,c_int32_t),int(npcol,c_int32_t),&
                                    int(deviceId,c_int32_t),CUDALIBMG_GRID_MAPPING_COL_MAJOR)
  if(ierr/=CUSOLVER_STATUS_SUCCESS) call errore(subname,'cusolverMgCreateDeviceGrid failed',10)

  !
  ! distribute the matrix on the process grid
  ! Initialize the array descriptors for the matrices A and B
  !
  if (nb == 0) then
    ndim_blc = int(ndim/npcol)
    if (ndim_blc*npcol < ndim ) ndim_blc=ndim_blc+1
    nblocks = 1
  else
    ndim_blc = nb
    nblocks = int(ndim/npcol/ndim_blc)
  endif
  lld = ndim
  !
  ierr = cusolverMgCreateMatrixDesc(descA,int(ndim,c_int64_t),int(ndim,c_int64_t), &
                                          int(ndim,c_int64_t),int(ndim_blc,c_int64_t),CUDA_R_64F,desc_grid)
  if(ierr/=CUSOLVER_STATUS_SUCCESS) call errore(subname,'cusolverMgCreateMatrixDesc on Amat failed',10)
  !
  ierr = cusolverMgCreateMatrixDesc(descAinv,int(ndim,c_int64_t),int(ndim,c_int64_t), &
                                          int(ndim,c_int64_t),int(ndim_blc,c_int64_t),CUDA_R_64F,desc_grid)
  if(ierr/=CUSOLVER_STATUS_SUCCESS) call errore(subname,'cusolverMgCreateMatrixDesc on Ainv failed',10)

  !
  ! initialize Ainv as the identity matrix
  !
  Ainv=0.0
  do i = 1, ndim
    Ainv(i,i)=1.0
  enddo
  !
  allocate(data_d(ndev))
  !
  allocate(Amat_p(ndev))
  allocate(Ainv_p(ndev))
  allocate(ipiv_p(ndev))
  !
  do id = 1, ndev
    !
    ierr = CudaSetDevice(id-1)
    allocate( data_d(id)%Amat(ndim,ndim_blc*nblocks) )
    allocate( data_d(id)%Ainv(ndim,ndim_blc*nblocks) )
    allocate( data_d(id)%ipiv(ndim) )
    Amat_p(id)=C_DEVLOC( data_d(id)%Amat )
    Ainv_p(id)=C_DEVLOC( data_d(id)%Ainv )
    ipiv_p(id)=C_DEVLOC( data_d(id)%ipiv )
    !
    data_d(id)%Amat=0.0d0
    data_d(id)%Ainv=0.0d0
    data_d(id)%ipiv=0
    !
  enddo

  !
  ! distribute the matrix A
  !
  ils=1
  ile=ndim
  !
  if (nb == 0) then
    !
    do id = 1, ndev
      !
      ierr = CudaSetDevice(id-1)
      !
      mycol=id-1
      !
      jls=mycol*ndim_blc+1
      jle=min((mycol+1)*ndim_blc,ndim)
      !
      data_d(id)%Amat=Amat(ils:ile,jls:jle)
      data_d(id)%Ainv=Ainv(ils:ile,jls:jle)
      !
    enddo
    !
  else
    !
    do id = 0, npcol-1
      !
      ierr = CudaSetDevice(id)
      !
      do ib = 0, nblocks-1
        !
        jgs = ib*npcol*ndim_blc + id*ndim_blc + 1
        jge = ib*npcol*ndim_blc + id*ndim_blc + ndim_blc
        jls = ib*ndim_blc + 1
        jle = ib*ndim_blc + ndim_blc
        !
        data_d(id+1)%Amat(ils:ile,jls:jle)=Amat(ils:ile,jgs:jge)
        data_d(id+1)%Ainv(ils:ile,jls:jle)=Ainv(ils:ile,jgs:jge)
        !
      enddo
    enddo
    !
  endif

  ! 
  ierr = cusolverMgGetrf_bufferSize(cusolvmg_h,ndim,ndim,Amat_p,1,1,descA,ipiv_p,CUDA_R_64F,lwork)
  if(ierr/=CUSOLVER_STATUS_SUCCESS) call errore(subname,'cusolverMgGetrf_bufferSize failed',10)
  !
  ierr = cusolverMgGetrs_bufferSize(cusolvmg_h,CUBLAS_OP_N,ndim,ndim,Amat_p,1,1,&
                                    descA,ipiv_p,Ainv_p,1,1,descAinv,CUDA_R_64F,lwork_tmp)
  if(ierr/=CUSOLVER_STATUS_SUCCESS) call errore(subname,'cusolverMgGetrs_bufferSize failed',10)
  !
  allocate(work_p(ndev))
  if (lwork_tmp > lwork) lwork = lwork_tmp
  do id = 1, ndev
    ierr = CudaSetDevice(id-1)
    allocate( data_d(id)%work(lwork) )
    work_p(id) = C_DEVLOC(data_d(id)%work)
  enddo
  !
  ierr = cudaDeviceSynchronize()
  if(ierr/=CUSOLVER_STATUS_SUCCESS) call errore(subname,'cudaDeviceSynchronize failed', 11)
  !
  ierr = cusolverMgGetrf(cusolvmg_h,ndim,ndim,Amat_p,1,1,descA,ipiv_p,CUDA_R_64F,work_p,lwork,info)
  if(ierr/=CUSOLVER_STATUS_SUCCESS) call errore(subname,'cusolverMgGetrf failed',info)
  !
  ierr = cudaDeviceSynchronize()
  if(ierr/=CUSOLVER_STATUS_SUCCESS) call errore(subname,'cudaDeviceSynchronize failed', 13)
  !
  ierr = cusolverMgGetrs(cusolvmg_h,CUBLAS_OP_N,ndim,ndim,Amat_p,1,1,&
                         descA,ipiv_p,Ainv_p,1,1,descAinv,CUDA_R_64F,work_p,lwork,info)
  if(ierr/=CUSOLVER_STATUS_SUCCESS) call errore(subname,'cusolverMgGetrs failed',info)
  !
  ierr = cudaDeviceSynchronize()
  if(ierr/=CUSOLVER_STATUS_SUCCESS) call errore(subname,'cudaDeviceSynchronize failed', 14)
  !
  ! gather the inverse matrix
  !
  Ainv=0.0d0
  if (nb == 0) then
    !
    do id = 1, ndev
      !
      mycol=id-1
      !
      ils=1
      ile=ndim
      jls=mycol*ndim_blc+1
      jle=min((mycol+1)*ndim_blc,ndim)
      !
      ierr = CudaSetDevice(id-1)
      Ainv(ils:ile,jls:jle)=data_d(id)%Ainv
      !
    enddo
    !
  else
    !
    ils = 1
    ile = ndim 
    do ib = 0, nblocks-1
      do id = 0, npcol-1
        !
        jgs = (ib * npcol * ndim_blc) + (id * ndim_blc) + 1
        jge = (ib * npcol * ndim_blc) + (id * ndim_blc) + ndim_blc
        jls = ib * ndim_blc + 1
        jle = (ib + 1) * ndim_blc
        !
        ierr = CudaSetDevice(id)
        Ainv(ils:ile,jgs:jge)=data_d(id+1)%Ainv(ils:ile,jls:jle)
        !
      end do
    end do
    !
  endif

  ! 
  ! local cleanup 
  ! 
  do id = 1, ndev
    !
    ierr = CudaSetDevice(id-1)
    deallocate( data_d(id)%Amat )
    deallocate( data_d(id)%Ainv )
    deallocate( data_d(id)%ipiv )
    deallocate( data_d(id)%work )
    !
  enddo
  !
  deallocate(data_d)
  deallocate(Amat_p)
  deallocate(Ainv_p)
  deallocate(ipiv_p)
  deallocate(work_p)
  !
  ierr = cusolverMgDestroyGrid(desc_grid)
  if(ierr/=CUSOLVER_STATUS_SUCCESS) call errore(subname,'cusolverMgDestroyGrid failed',abs(ierr)+1)
  ierr = cusolverMgDestroyMatrixDesc(descA)
  if(ierr/=CUSOLVER_STATUS_SUCCESS) call errore(subname,'cusolverMgDestroyMatrixDesc Amat failed',abs(ierr)+1)
  ierr = cusolverMgDestroyMatrixDesc(descAinv)
  if(ierr/=CUSOLVER_STATUS_SUCCESS) call errore(subname,'cusolverMgDestroyMatrixDesc Ainv failed',abs(ierr)+1)
  !
  !
  ! compute and report eigenvalues of Ainv
  !
  allocate( w(ndim), zmat(ndim,ndim) )
  call mat_hdiag( zmat, w, Ainv, ndim )
  !
  if ( me_pool == root_pool ) then
     write(*,"(5x,i4,' Ainv Eigenvalues')") my_pool_id
     !
     ! print the 8 largest eigs
     write(*,"(5x,i4,100f15.9)") my_pool_id, w(ndim:max(1,ndim-7):-1)
     ! print the 8 smallest eigs
     write(*,"(5x,i4,100f15.9)") my_pool_id, w(1:min(ndim,8))
     call flush()
  endif
  !
  deallocate( w, zmat )
  !
#endif
  !
  return
  !
end subroutine para_inverse_GPU

