
!===========================================
  subroutine inverse_check( ndim, Amat, Ainv )
  !===========================================
  use kinds
  use util_module
  use para_module
  implicit none
  !
  integer   :: ndim
  real(dbl) :: Amat(ndim,ndim)
  real(dbl) :: Ainv(ndim,ndim)
  !
  integer   :: i, j, ierr
  logical   :: lerror
  real(dbl) :: toll=1.0d-10
  real(dbl), allocatable :: zmat(:,:)
  !
  allocate( zmat(ndim,ndim) )
  !
  call mat_mul( zmat, Amat, 'N', Ainv, 'N', ndim,ndim,ndim)
  !
  lerror = .false.
  outer_loop:&
  do j = 1, ndim
  do i = j+1, ndim
      if ( abs( zmat(i,j) ) > toll ) then 
          lerror = .true.
          exit outer_loop
      endif
  enddo
  enddo outer_loop
  !if ( lerror ) call errore('inverse_check','A * Ainv /= Id',10)
  !
  do i = 1, ndim
      if ( abs( zmat(i,i)-1.0d0 ) > toll ) then 
          lerror = .true.
          exit
      endif
  enddo
  !if ( lerror ) call errore('inverse_check','A * Ainv /= Id',11)
  !
  if ( .not. lerror ) then 
     if (me_pool==root_pool) write(*,"(5x,i2,' Inverse_check:   passed')") my_pool_id
  else
     if (me_pool==root_pool) write(*,"(5x,i2,' Inverse_check:   failed')") my_pool_id
  endif
  if (me_pool==root_pool) call flush()
  !
  deallocate( zmat )
  !
#ifdef __MPI
  call MPI_barrier( mpi_comm_world, ierr)
#endif
  !
  return
  !
end subroutine inverse_check

