
!===============
program mytest
  !===============
  ! 
  ! simple program to invert a NxN matrix using scalapack 
  !
  use kinds
  use constants
  use para_module
  use dev_utils_m
  implicit none
  !
  integer        :: iargc              ! function giving no of arguments
  integer        :: nargs
  character(256) :: str, str1 
  character(6)   :: subname="mytest"
  !
  integer        :: ndim, nprow, npcol, ndim_blc
  integer        :: ivar, serial, parallel, serial_gpu, parallel_gpu
  real(dbl),allocatable :: Amat(:,:), Ainv(:,:)
  real(dbl),allocatable :: Amat_d(:,:), Ainv_d(:,:)
#ifdef __CUDA 
  attributes(device) :: Amat_d, Ainv_d
#endif
  real(dbl)      :: t0,t1
  integer        :: ind, ierr

!===================

  ionode=.true.
  ionode_id=0
  !
  !
#ifdef __MPI
  !
  call MPI_INIT(ierr)
  call MPI_COMM_SIZE(MPI_COMM_WORLD,nproc,ierr)
  call MPI_COMM_RANK(MPI_COMM_WORLD,mpime,ierr)
  !
  ionode=.false.
  if (mpime==0) ionode=.true.
  if (ionode) ionode_id=mpime
  !
#endif
  ! 
  !
  ! get cmd line options
  !
  nargs = COMMAND_ARGUMENT_COUNT ()
  if ( nargs < 1 ) call errore(subname,'invalid # of args',10)
  !
  ndim=500
  nprow=1
  npcol=1
  npool=1
  serial=0
  parallel=1
  serial_gpu=1
  parallel_gpu=1
  ndim_blc=0
  !
  ind=1
  do while (ind <= nargs)
     !
     call get_command_argument(ind,str) ! e.g. --npool...
     call get_command_argument(ind+1,str1) ! e.g. number of pools
     read(str1,*,iostat=ierr) ivar
     if ( ierr /=0 ) call errore(subname,'reading arg',10)
     !
     select case ( trim(str) )
     case ("-n", "--ndim") 
          ndim=ivar
     case ("-nr","--nrow") 
          nprow=ivar          !ivar
     case ("-nc","--ncol") 
          npcol=ivar          !1
     case ("-np", "--npool") 
          npool=ivar          !1
     case ("-nb", "--ndim_block") 
          ndim_blc=ivar 
     case ("-s", "--serial") 
          serial=ivar
     case ("-p", "--parallel") 
          parallel=ivar
     case ("-sg", "--serial_gpu") 
          serial_gpu=ivar
     case ("-pg", "--parallel_gpu") 
          parallel_gpu=ivar
     end select
     !
     ind=ind+2
     !
  enddo 
  !
  np_ortho(1)=nprow
  np_ortho(2)=npcol
  !
  ! report
  !
  if ( ionode ) then
     write(*,"(/,2x,'INPUT report')")
     write(*,"(2x,'      ndim : ',i5)") ndim
     write(*,"(2x,'ndim_block : ',i5)") ndim_blc
     write(*,"(2x,'      ncpu : ',i5)") nproc
     write(*,"(2x,' para grid : ',3x,i2,' x',i2)") nprow,npcol
     write(*,"(2x,'     npool : ',i5)") npool
     write(*,"()")
  endif
  !
  if ( nprow*npcol*npool /= nproc ) CALL errore(subname,"invalid number of MPI tasks",10)
  if ( ndim_blc /= 0 .and. mod(ndim,ndim_blc) /= 0 ) CALL errore(subname,"invalid number of ndim_blc",10)
  !
  ! setup parallel structure
  !
  call parallel_structure()

  !
  ! workspace
  !
  allocate( Amat(ndim,ndim) )
  allocate( Ainv(ndim,ndim) )
  !
#ifdef __CUDA
  allocate( Amat_d(ndim,ndim) )
  allocate( Ainv_d(ndim,ndim) )
#endif
  !
  ! init
  !
  if (ionode) write(*,"(/,1x,'Matrix Building')") 
  t0=wallclock()
  call build_matrix( ndim, Amat )
  t1=wallclock()
  if (ionode) write(*,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !
  if (serial/=0) then
    !
    ! serial inversion
    !
    if (ionode) write(*,"(/,1x,'Serial Inversion')") 
    if (ionode) call flush()
    !
    t0=wallclock()
    !
    call serial_inverse( ndim, Amat, Ainv )
    !
    t1=wallclock()
    !
    if (ionode) write(*,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
    if (ionode) write(*,"(/,3x,'Check A*Ainv=Id')") 
    if (ionode) call flush() 
    call inverse_check( ndim, Amat, Ainv )
    !
  endif
  !
  if (serial_gpu/=0) then
    !
    ! serial inversion GPU
    !
#ifdef __CUDA
    if (ionode) write(*,"(/,1x,'Serial Inversion GPU')") 
    if (ionode) call flush()
    !
    t0=wallclock()
    Amat_d = Amat
    !
    call serial_inverse_GPU( ndim, Amat_d, Ainv_d )
    !
    Ainv = Ainv_d
    !
    t1=wallclock()
    !
    if (ionode) write(*,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
    if (ionode) write(*,"(/,3x,'Check A*Ainv=Id')") 
    if (ionode) call flush() 
    call inverse_check( ndim, Amat, Ainv )
#endif
  !
  endif
  !
  if (parallel/=0) then
    !
    ! parallel inversion
    !
#ifdef __MPI
    call MPI_barrier( mpi_comm_world, ierr)
#endif
#ifdef __SCALAPACK
    if (ionode) write(*,"(/,1x,'Parallel Inversion')") 
    if (ionode) call flush()
    !
    t0=wallclock()
    !
    call para_inverse( ndim, Amat, Ainv )
    !
    t1=wallclock()
    if (ionode) write(*,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
    !
    if (ionode) write(*,"(/,3x,'Check A*Ainv=Id')") 
    if (ionode) call flush() 
    call inverse_check( ndim, Amat, Ainv )
#endif
    !
  endif
  !
  if (parallel_gpu/=0) then
    !
    ! parallel inversion GPU
    !
#ifdef __MPI
    call MPI_barrier( mpi_comm_world, ierr)
#endif
#if defined __CUDA && defined __CUSOLVER_MG
    if (ionode) write(*,"(/,1x,'Parallel Inversion GPU')") 
    if (ionode) call flush()
    !
    t0=wallclock()
    !
    call para_inverse_GPU( ndim, Amat, Ainv, ndim_blc )
    !
    t1=wallclock()
    if (ionode) write(*,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
    !
    if (ionode) write(*,"(/,3x,'Check A*Ainv=Id')") 
    if (ionode) call flush() 
    call inverse_check( ndim, Amat, Ainv )
#endif
  !
  endif
  !
  ! cleanup
  !
  deallocate( Amat, Ainv )

#ifdef __MPI
  call MPI_FINALIZE(ierr)
#endif
  !
end program mytest
  
