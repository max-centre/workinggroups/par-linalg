!
! Copyright (c) 2016, NVIDIA CORPORATION. All rights reserved.
! 
! 
! Permission is hereby granted, free of charge, to any person obtaining a
! copy of this software and associated documentation files (the "Software"),
! to deal in the Software without restriction, including without limitation
! the rights to use, copy, modify, merge, publish, distribute, sublicense,
! and/or sell copies of the Software, and to permit persons to whom the
! Software is furnished to do so, subject to the following conditions:
! 
! The above copyright notice and this permission notice shall be included in
! all copies or substantial portions of the Software.
! 
! THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
! IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
! FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
! THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
! LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
! FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
! DEALINGS IN THE SOFTWARE.
!
module cusolver_m
  !
  use iso_c_binding
  !
#ifdef __CUDA
  ! enums
  enum, bind(C) ! cusolverStatus_t
     enumerator :: CUSOLVER_STATUS_SUCCESS=0
     enumerator :: CUSOLVER_STATUS_NOT_INITIALIZED=1
     enumerator :: CUSOLVER_STATUS_ALLOC_FAILED=2
     enumerator :: CUSOLVER_STATUS_INVALID_VALUE=3
     enumerator :: CUSOLVER_STATUS_ARCH_MISMATCH=4
     enumerator :: CUSOLVER_STATUS_MAPPING_ERROR=5
     enumerator :: CUSOLVER_STATUS_EXECUTION_FAILED=6
     enumerator :: CUSOLVER_STATUS_INTERNAL_ERROR=7
     enumerator :: CUSOLVER_STATUS_MATRIX_TYPE_NOT_SUPPORTED=8
     enumerator :: CUSOLVER_STATUS_NOT_SUPPORTED = 9
     enumerator :: CUSOLVER_STATUS_ZERO_PIVOT=10
     enumerator :: CUSOLVER_STATUS_INVALID_LICENSE=11
  end enum

  enum, bind(c) ! cusolverEigType_t
     enumerator :: CUSOLVER_EIG_TYPE_1=1
     enumerator :: CUSOLVER_EIG_TYPE_2=2
     enumerator :: CUSOLVER_EIG_TYPE_3=3
  end enum

  enum, bind(c) ! cusolverEigMode_t
     enumerator :: CUSOLVER_EIG_MODE_NOVECTOR=0
     enumerator :: CUSOLVER_EIG_MODE_VECTOR=1
  end enum

  ! types

  type cusolverDnHandle
     type(c_ptr) :: handle
  end type cusolverDnHandle

  ! ---------
  ! functions
  ! ---------

  interface
     integer(c_int) function cusolverDnCreate(handle) &
          bind(C,name='cusolverDnCreate')
       import cusolverDnHandle
       type(cusolverDnHandle) :: handle
     end function cusolverDnCreate
  end interface

  interface
     integer(c_int) function cusolverDnDestroy(handle) &
          bind(C,name='cusolverDnDestroy')
       import cusolverDnHandle
       type(cusolverDnHandle), value :: handle
     end function cusolverDnDestroy
  end interface

  interface
     integer(c_int) function cusolverDnSetStream(handle, stream) &
          bind(C,name='cusolverDnSetStream')
       use cudafor
       import cusolverDnHandle
       type(cusolverDnHandle), value :: handle
       integer(cuda_stream_kind), value :: stream
     end function cusolverDnSetStream
  end interface

  interface
     integer(c_int) function cusolverDnZpotrf_bufferSize( &
          handle, uplo, n, A, lda, Lwork) &
          bind(C, name='cusolverDnZpotrf_bufferSize') 
       use iso_c_binding
       import cusolverDnHandle
       type(cusolverDnHandle), value :: handle 
       integer(c_int), value :: uplo 
       integer(c_int), value :: n 
       complex(8), device :: A(*) 
       integer(c_int), value :: lda 
       integer(c_int) :: Lwork
     end function cusolverDnZpotrf_bufferSize
  end interface

  interface
     integer(c_int) function cusolverDnDpotrf_bufferSize( &
          handle, uplo, n, A, lda, Lwork) &
          bind(C, name='cusolverDnDpotrf_bufferSize') 
       use iso_c_binding
       import cusolverDnHandle
       type(cusolverDnHandle), value :: handle 
       integer(c_int), value :: uplo 
       integer(c_int), value :: n 
       real(8), device :: A(*) 
       integer(c_int), value :: lda 
       integer(c_int) :: Lwork
     end function cusolverDnDpotrf_bufferSize
  end interface

  interface
     integer(c_int) function cusolverDnZpotrf( &
          handle, uplo, n, A, lda, Workspace, Lwork, devInfo) &
          bind(C,name='cusolverDnZpotrf') 
       use iso_c_binding
       import cusolverDnHandle
       type(cusolverDnHandle), value :: handle 
       integer(c_int), value :: uplo 
       integer(c_int), value :: n 
       complex(8), device :: A(*) 
       integer(c_int), value :: lda 
       !pgi$ ignore_tkr (r) Workspace
       complex(8), device :: Workspace(*)
       integer(c_int), value :: Lwork
       integer(c_int), device :: devInfo
     end function cusolverDnZpotrf
  end interface

  interface
     integer(c_int) function cusolverDnDpotrf( &
          handle, uplo, n, A, lda, Workspace, Lwork, devInfo) &
          bind(C,name='cusolverDnDpotrf') 
       use iso_c_binding
       import cusolverDnHandle
       type(cusolverDnHandle), value :: handle 
       integer(c_int), value :: uplo 
       integer(c_int), value :: n 
       real(8), device :: A(*) 
       integer(c_int), value :: lda 
       !pgi$ ignore_tkr (r) Workspace
       real(8), device :: Workspace(*)
       integer(c_int), value :: Lwork
       integer(c_int), device :: devInfo
     end function cusolverDnDpotrf
  end interface

  interface
     integer(c_int) function cusolverDnZhegvd_bufferSize( &
          handle, itype, jobz, uplo, n, A, lda, B, ldb, W, Lwork) &
          bind(C, name='cusolverDnZhegvd_bufferSize')
       use iso_c_binding
       import cusolverDnHandle
       type(cusolverDnHandle), value :: handle
       integer(c_int), value :: itype
       integer(c_int), value :: jobz
       integer(c_int), value :: uplo
       integer(c_int), value :: n
       complex(8), device :: A(*)
       integer(c_int), value :: lda
       complex(8), device :: B(*)
       integer(c_int), value :: ldb
       real(8), device :: W(*)
       integer(c_int) :: Lwork
     end function cusolverDnZhegvd_bufferSize
  end interface

  interface
     integer(c_int) function cusolverDnDsygvd_bufferSize( &
          handle, itype, jobz, uplo, n, A, lda, B, ldb, W, Lwork) &
          bind(C, name='cusolverDnDsygvd_bufferSize')
       use iso_c_binding
       import cusolverDnHandle
       type(cusolverDnHandle), value :: handle
       integer(c_int), value :: itype
       integer(c_int), value :: jobz
       integer(c_int), value :: uplo
       integer(c_int), value :: n
       real(8), device :: A(*)
       integer(c_int), value :: lda
       real(8), device :: B(*)
       integer(c_int), value :: ldb
       real(8), device :: W(*)
       integer(c_int) :: Lwork
     end function cusolverDnDsygvd_bufferSize
  end interface

  interface
     integer(c_int) function cusolverDnZhegvd( &
          handle, itype, jobz, uplo, n, A, lda, B, ldb, W, Workspace, Lwork, devInfo) &
          bind(C,name='cusolverDnZhegvd')
       use iso_c_binding
       import cusolverDnHandle
       type(cusolverDnHandle), value :: handle
       integer(c_int), value :: itype
       integer(c_int), value :: jobz
       integer(c_int), value :: uplo
       integer(c_int), value :: n
       complex(8), device :: A(*)
       integer(c_int), value :: lda
       complex(8), device :: B(*)
       integer(c_int), value :: ldb
       real(8), device :: W(*)
       !pgi$ ignore_tkr (r) Workspace
       complex(8), device :: Workspace(*)
       integer(c_int), value :: Lwork
       integer(c_int), device :: devInfo
     end function cusolverDnZhegvd
  end interface

  interface
     integer(c_int) function cusolverDnDsygvd( &
          handle, itype, jobz, uplo, n, A, lda, B, ldb, W, Workspace, Lwork, devInfo) &
          bind(C,name='cusolverDnDsygvd')
       use iso_c_binding
       import cusolverDnHandle
       type(cusolverDnHandle), value :: handle
       integer(c_int), value :: itype
       integer(c_int), value :: jobz
       integer(c_int), value :: uplo
       integer(c_int), value :: n
       real(8), device :: A(*)
       integer(c_int), value :: lda
       real(8), device :: B(*)
       integer(c_int), value :: ldb
       real(8), device :: W(*)
       !pgi$ ignore_tkr (r) Workspace
       real(8), device :: Workspace(*)
       integer(c_int), value :: Lwork
       integer(c_int), device :: devInfo
     end function cusolverDnDsygvd
  end interface

  interface
     integer(c_int) function cusolverDnZheevd_bufferSize( &
          handle, jobz, uplo, n, A, lda, W, Lwork) &
          bind(C, name='cusolverDnZheevd_bufferSize')
       use iso_c_binding
       import cusolverDnHandle
       type(cusolverDnHandle), value :: handle
       integer(c_int), value :: jobz
       integer(c_int), value :: uplo
       integer(c_int), value :: n
       complex(8), device :: A(*)
       integer(c_int), value :: lda
       real(8), device :: W(*)
       integer(c_int) :: Lwork
     end function cusolverDnZheevd_bufferSize
  end interface

  interface
     integer(c_int) function cusolverDnZheevd( &
          handle, jobz, uplo, n, A, lda, W, Workspace, Lwork, devInfo) &
          bind(C,name='cusolverDnZheevd')
       use iso_c_binding
       import cusolverDnHandle
       type(cusolverDnHandle), value :: handle
       integer(c_int), value :: jobz
       integer(c_int), value :: uplo
       integer(c_int), value :: n
       complex(8), device :: A(*)
       integer(c_int), value :: lda
       real(8), device :: W(*)
       !pgi$ ignore_tkr (r) Workspace
       complex(8), device :: Workspace(*)
       integer(c_int), value :: Lwork
       integer(c_int), device :: devInfo
     end function cusolverDnZheevd
  end interface

  interface
     integer(c_int) function cusolverDnCgetrf_bufferSize( &
          handle, m, n, A, lda, Lwork) &
          bind(C, name='cusolverDnCgetrf_bufferSize') 
       use iso_c_binding
       import cusolverDnHandle
       type(cusolverDnHandle), value :: handle 
       integer(c_int), value :: m
       integer(c_int), value :: n
       complex(4), device :: A(lda,*) 
       integer(c_int), value :: lda 
       integer(c_int) :: Lwork
     end function cusolverDnCgetrf_bufferSize
  end interface

  interface
     integer(c_int) function cusolverDnCgetrf( &
          handle, m, n, A, lda, Workspace, devIpiv, devInfo) &
          bind(C,name='cusolverDnCgetrf')
       use iso_c_binding
       import cusolverDnHandle
       type(cusolverDnHandle), value :: handle
       integer(c_int), value :: m
       integer(c_int), value :: n
       complex(4), device :: A(lda,*)
       integer(c_int), value :: lda
       !pgi$ ignore_tkr (r) Workspace
       complex(4), device :: Workspace(*)
       integer(c_int), device :: devIpiv(*)
       integer(c_int), device :: devInfo(*)
     end function cusolverDnCgetrf
  end interface

  interface
     integer(c_int) function cusolverDnCgetrs( &
          handle, trans, n, nrhs, A, lda, devIpiv, B, ldb, devInfo) &
          bind(C,name='cusolverDnCgetrs')
       use iso_c_binding
       import cusolverDnHandle
       type(cusolverDnHandle), value :: handle
       integer(c_int), value :: trans
       integer(c_int), value :: n
       integer(c_int), value :: nrhs
       complex(4), device :: A(lda,*)
       integer(c_int), value :: lda
       integer(c_int), device :: devIpiv(*)
       complex(4), device :: B(ldb,*)
       integer(c_int), value :: ldb
       integer(c_int), device :: devInfo(*)
     end function cusolverDnCgetrs
  end interface

  interface
     integer(c_int) function cusolverDnZgetrf_bufferSize( &
          handle, m, n, A, lda, Lwork) &
          bind(C, name='cusolverDnZgetrf_bufferSize') 
       use iso_c_binding
       import cusolverDnHandle
       type(cusolverDnHandle), value :: handle 
       integer(c_int), value :: m
       integer(c_int), value :: n
       complex(8), device :: A(lda,*) 
       integer(c_int), value :: lda 
       integer(c_int) :: Lwork
     end function cusolverDnZgetrf_bufferSize
  end interface

  interface
     integer(c_int) function cusolverDnZgetrf( &
          handle, m, n, A, lda, Workspace, devIpiv, devInfo) &
          bind(C,name='cusolverDnZgetrf')
       use iso_c_binding
       import cusolverDnHandle
       type(cusolverDnHandle), value :: handle
       integer(c_int), value :: m
       integer(c_int), value :: n
       complex(8), device :: A(lda,*)
       integer(c_int), value :: lda
       !pgi$ ignore_tkr (r) Workspace
       complex(8), device :: Workspace(*)
       integer(c_int), device :: devIpiv(*)
       integer(c_int), device :: devInfo(*)
     end function cusolverDnZgetrf
  end interface

  interface
     integer(c_int) function cusolverDnZgetrs( &
          handle, trans, n, nrhs, A, lda, devIpiv, B, ldb, devInfo) &
          bind(C,name='cusolverDnZgetrs')
       use iso_c_binding
       import cusolverDnHandle
       type(cusolverDnHandle), value :: handle
       integer(c_int), value :: trans
       integer(c_int), value :: n
       integer(c_int), value :: nrhs
       complex(8), device :: A(lda,*)
       integer(c_int), value :: lda
       integer(c_int), device :: devIpiv(*)
       complex(8), device :: B(ldb,*)
       integer(c_int), value :: ldb
       integer(c_int), device :: devInfo(*)
     end function cusolverDnZgetrs
  end interface

  interface
     integer(c_int) function cusolverDnDgetrf_bufferSize( &
          handle, m, n, A, lda, Lwork) &
          bind(C, name='cusolverDnDgetrf_bufferSize') 
       use iso_c_binding
       import cusolverDnHandle
       type(cusolverDnHandle), value :: handle 
       integer(c_int), value :: m
       integer(c_int), value :: n
       real(8), device :: A(lda,*) 
       integer(c_int), value :: lda 
       integer(c_int) :: Lwork
     end function cusolverDnDgetrf_bufferSize
  end interface

  interface
     integer(c_int) function cusolverDnDgetrf( &
          handle, m, n, A, lda, Workspace, devIpiv, devInfo) &
          bind(C,name='cusolverDnDgetrf')
       use iso_c_binding
       import cusolverDnHandle
       type(cusolverDnHandle), value :: handle
       integer(c_int), value :: m
       integer(c_int), value :: n
       real(8), device :: A(lda,*)
       integer(c_int), value :: lda
       !pgi$ ignore_tkr (r) Workspace
       real(8), device :: Workspace(*)
       integer(c_int), device :: devIpiv(*)
       integer(c_int), device :: devInfo(*)
     end function cusolverDnDgetrf
  end interface

  interface
     integer(c_int) function cusolverDnDgetrs( &
          handle, trans, n, nrhs, A, lda, devIpiv, B, ldb, devInfo) &
          bind(C,name='cusolverDnDgetrs')
       use iso_c_binding
       import cusolverDnHandle
       type(cusolverDnHandle), value :: handle
       integer(c_int), value :: trans
       integer(c_int), value :: n
       integer(c_int), value :: nrhs
       real(8), device :: A(lda,*)
       integer(c_int), value :: lda
       integer(c_int), device :: devIpiv(*)
       real(8), device :: B(ldb,*)
       integer(c_int), value :: ldb
       integer(c_int), device :: devInfo(*)
     end function cusolverDnDgetrs
  end interface

#endif

#if defined __CUDA && defined __CUSOLVER_MG

  enum, bind(c) ! cusolverMgGridMapping_t
     enumerator :: CUDALIBMG_GRID_MAPPING_ROW_MAJOR=1
     enumerator :: CUDALIBMG_GRID_MAPPING_COL_MAJOR=0
  end enum
  
!  enum, bind(C) ! cudaDataType_t
!     enumerator :: CUDA_R_16F  =  2   ! real as a half
!     enumerator :: CUDA_C_16F  =  6   ! complex as a pair of half numbers
!     enumerator :: CUDA_R_16BF = 14   ! real as a nv_bfloat16
!     enumerator :: CUDA_C_16BF = 15   ! complex as a pair of nv_bfloat16 numbers
!     enumerator :: CUDA_R_32F  =  0   ! real as a float
!     enumerator :: CUDA_C_32F  =  4   ! complex as a pair of float numbers
!     enumerator :: CUDA_R_64F  =  1   ! real as a double
!     enumerator :: CUDA_C_64F  =  5   ! complex as a pair of double numbers
!     enumerator :: CUDA_R_4I   = 16   ! real as a signed 4-bit int
!     enumerator :: CUDA_C_4I   = 17   ! complex as a pair of signed 4-bit int numbers
!     enumerator :: CUDA_R_4U   = 18   ! real as a unsigned 4-bit int
!     enumerator :: CUDA_C_4U   = 19   ! complex as a pair of unsigned 4-bit int numbers
!     enumerator :: CUDA_R_8I   =  3   ! real as a signed 8-bit int
!     enumerator :: CUDA_C_8I   =  7   ! complex as a pair of signed 8-bit int numbers
!     enumerator :: CUDA_R_8U   =  8   ! real as a unsigned 8-bit int
!     enumerator :: CUDA_C_8U   =  9   ! complex as a pair of unsigned 8-bit int numbers
!     enumerator :: CUDA_R_16I  = 20   ! real as a signed 16-bit int
!     enumerator :: CUDA_C_16I  = 21   ! complex as a pair of signed 16-bit int numbers
!     enumerator :: CUDA_R_16U  = 22   ! real as a unsigned 16-bit int
!     enumerator :: CUDA_C_16U  = 23   ! complex as a pair of unsigned 16-bit int numbers
!     enumerator :: CUDA_R_32I  = 10   ! real as a signed 32-bit int
!     enumerator :: CUDA_C_32I  = 11   ! complex as a pair of signed 32-bit int numbers
!     enumerator :: CUDA_R_32U  = 12   ! real as a unsigned 32-bit in
!     enumerator :: CUDA_C_32U  = 13   ! complex as a pair of unsigned 32-bit int numbers
!     enumerator :: CUDA_R_64I  = 24   ! real as a signed 64-bit int
!     enumerator :: CUDA_C_64I  = 25   ! complex as a pair of signed 64-bit int numbers
!     enumerator :: CUDA_R_64U  = 26   ! real as a unsigned 64-bit int
!     enumerator :: CUDA_C_64U  = 27   ! complex as a pair of unsigned 64-bit int numbers
!  end enum

  type cusolverMgHandle
     type(c_ptr) :: handle
  end type cusolverMgHandle
  !
  type cusolverMgGrid_t
     type(c_ptr) :: grid
  end type cusolverMgGrid_t
  !
  type cusolverMgMatrixDesc_t
     type(c_ptr) :: matrix_desc
  end type cusolverMgMatrixDesc_t

  interface
     integer(c_int) function cusolverMgCreate(handle) &
          bind(C,name='cusolverMgCreate')
       import cusolverMgHandle
       type(cusolverMgHandle) :: handle
     end function cusolverMgCreate
  end interface

  interface
     integer(c_int) function cusolverMgDestroy(handle) &
          bind(C,name='cusolverMgDestroy')
       import cusolverMgHandle
       type(cusolverMgHandle), value :: handle
     end function cusolverMgDestroy
  end interface

  interface
     integer(c_int) function cusolverMgDeviceSelect(handle,nbDevices,deviceId) &
          bind(C,name='cusolverMgDeviceSelect')
       use iso_c_binding
       import cusolverMgHandle
       type(cusolverMgHandle), value :: handle
       integer(c_int), value :: nbDevices
       integer(c_int)        :: deviceId(*)
     end function cusolverMgDeviceSelect
  end interface

  interface
     integer(c_int) function cusolverMgCreateDeviceGrid( &
          grid, numRowDevices, numColDevices, deviceId, mapping) &
          bind(C,name='cusolverMgCreateDeviceGrid')
       use iso_c_binding
       import cusolverMgGrid_t
       type(cusolverMgGrid_t) :: grid
       integer(c_int32_t), value :: numRowDevices
       integer(c_int32_t), value :: numColDevices
       integer(c_int32_t)        :: deviceId(*)
       integer(c_int),     value :: mapping
     end function cusolverMgCreateDeviceGrid
  end interface

  interface
     integer(c_int) function cusolverMgDestroyGrid(grid) &
          bind(C,name='cusolverMgDestroyGrid')
       use iso_c_binding
       import cusolverMgGrid_t
       type(cusolverMgGrid_t), value :: grid
     end function cusolverMgDestroyGrid
  end interface

  interface
     integer(c_int) function cusolverMgCreateMatrixDesc( &
          desc, numRows, numCols, rowBlockSize, colBlockSize, dataType, grid) &
          bind(C,name='cusolverMgCreateMatrixDesc')
       use iso_c_binding
       import cusolverMgMatrixDesc_t
       import cusolverMgGrid_t
       type(cusolverMgMatrixDesc_t) :: desc
       integer(c_int64_t), value :: numRows
       integer(c_int64_t), value :: numCols
       integer(c_int64_t), value :: rowBlockSize
       integer(c_int64_t), value :: colBlockSize
       integer(c_int),     value :: dataType
       type(cusolverMgGrid_t), value :: grid
     end function cusolverMgCreateMatrixDesc
  end interface

  interface
     integer(c_int) function cusolverMgDestroyMatrixDesc(desc) &
          bind(C,name='cusolverMgDestroyMatrixDesc')
       use iso_c_binding
       import cusolverMgMatrixDesc_t
       type(cusolverMgMatrixDesc_t), value :: desc
     end function cusolverMgDestroyMatrixDesc
  end interface
  
  interface
     integer(c_int) function cusolverMgGetrf_bufferSize( &
          handle, m, n, array_d_A, iA, jA, descrA, array_d_IPIV, computeType, lwork) &
          bind(C, name='cusolverMgGetrf_bufferSize') 
       use iso_c_binding
       use cudafor
       import cusolverMgHandle
       import cusolverMgMatrixDesc_t
       type(cusolverMgHandle), value :: handle 
       integer(c_int), value :: m
       integer(c_int), value :: n
       type(c_devptr)        :: array_d_A(*)
       integer(c_int), value :: iA
       integer(c_int), value :: jA
       type(cusolverMgMatrixDesc_t), value :: descrA
       type(c_devptr)        :: array_d_IPIV(*)
       integer(c_int), value :: computeType
       integer(c_int64_t)    :: lwork
     end function cusolverMgGetrf_bufferSize
  end interface

  interface
     integer(c_int) function cusolverMgGetrf( &
          handle, m, n, array_d_A, iA, jA, descrA, array_d_IPIV, computeType, &
          array_d_work, lwork, info ) &
          bind(C, name='cusolverMgGetrf') 
       use iso_c_binding
       use cudafor
       import cusolverMgHandle
       import cusolverMgMatrixDesc_t
       type(cusolverMgHandle), value :: handle 
       integer(c_int), value :: m
       integer(c_int), value :: n
       type(c_devptr)        :: array_d_A(*)
       integer(c_int), value :: iA
       integer(c_int), value :: jA
       type(cusolverMgMatrixDesc_t), value :: descrA
       type(c_devptr)        :: array_d_IPIV(*)
       integer(c_int), value :: computeType
       type(c_devptr)        :: array_d_work(*)
       integer(c_int64_t), value :: lwork
       integer(c_int)        :: info
     end function cusolverMgGetrf
  end interface

  interface
     integer(c_int) function cusolverMgGetrs_buffersize( &
          handle, trans, n, nrhs, array_d_A, iA, jA, descrA, array_d_IPIV, &
          array_d_B, iB, jB, descrB, computeType, lwork ) &
          bind(C, name='cusolverMgGetrs_bufferSize') 
       use iso_c_binding
       use cudafor
       import cusolverMgHandle
       import cusolverMgMatrixDesc_t
       type(cusolverMgHandle), value :: handle 
       integer(c_int), value :: trans
       integer(c_int), value :: n
       integer(c_int), value :: nrhs
       type(c_devptr)        :: array_d_A(*)
       integer(c_int), value :: iA
       integer(c_int), value :: jA
       type(cusolverMgMatrixDesc_t), value :: descrA
       type(c_devptr)        :: array_d_IPIV(*)
       type(c_devptr)        :: array_d_B(*)
       integer(c_int), value :: iB
       integer(c_int), value :: jB
       type(cusolverMgMatrixDesc_t), value :: descrB
       integer(c_int), value :: computeType
       integer(c_int64_t)    :: lwork
     end function cusolverMgGetrs_buffersize
  end interface

  interface
     integer(c_int) function cusolverMgGetrs( &
          handle, trans, n, nrhs, array_d_A, iA, jA, descrA, array_d_IPIV, &
          array_d_B, iB, jB, descrB, computeType, array_d_work, lwork, info ) &
          bind(C, name='cusolverMgGetrs') 
       use iso_c_binding
       use cudafor
       import cusolverMgHandle
       import cusolverMgMatrixDesc_t
       type(cusolverMgHandle), value :: handle 
       integer(c_int), value :: trans
       integer(c_int), value :: n
       integer(c_int), value :: nrhs
       type(c_devptr)        :: array_d_A(*)
       integer(c_int), value :: iA
       integer(c_int), value :: jA
       type(cusolverMgMatrixDesc_t), value :: descrA
       type(c_devptr)        :: array_d_IPIV(*)
       type(c_devptr)        :: array_d_B(*)
       integer(c_int), value :: iB
       integer(c_int), value :: jB
       type(cusolverMgMatrixDesc_t), value :: descrB
       integer(c_int), value :: computeType
       type(c_devptr)        :: array_d_work(*)
       integer(c_int64_t), value :: lwork
       integer(c_int)        :: info
     end function cusolverMgGetrs
  end interface

#endif

end module cusolver_m

