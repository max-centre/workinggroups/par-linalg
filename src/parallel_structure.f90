  
!===========================================
  subroutine parallel_structure( )
  !===========================================
  use kinds
  use para_module
  implicit none
  !
#ifdef __MPI
  world_comm = MPI_COMM_WORLD
  !
  ! init pools
  call init_pools( world_comm )
  !
  ! init the scalapack grid
  call init_ortho( product(np_ortho), intra_pool_comm )
  !
#endif
  !
  return
  !
end subroutine parallel_structure

