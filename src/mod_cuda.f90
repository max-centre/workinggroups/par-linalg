module cuda_m
#ifdef __CUDA
  use cudafor
  use cublas
  use cusolver_m
#endif
implicit none
!
!
logical:: cuda_linalg_init=.false.
#ifdef __CUDA
  type(cusolverDnHandle)   :: cusolv_h
  public :: cusolv_h
#endif
#if defined __CUDA && defined __CUSOLVER_MG
  type(cusolverMgHandle)   :: cusolvmg_h
  public :: cusolvmg_h
#endif
!
!
public :: cuda_linalg_init
public :: cuda_linalg_setup
!
!
contains
!
subroutine cuda_linalg_setup()
    implicit none
#ifdef __CUDA
    integer :: istat 
    istat = cublasInit()
    if(istat/=0) call errore('cuda_linalg_setup','cublasInit failed',10)
    istat = cusolverDnCreate(cusolv_h)
    if(istat/=CUSOLVER_STATUS_SUCCESS) call errore('cuda_linalg_setup','cusolverDnCreate failed',10)
#endif
#if defined __CUDA && __CUSOLVER_MG
    istat = cusolverMgCreate(cusolvmg_h)
    if(istat/=CUSOLVER_STATUS_SUCCESS) call errore('cuda_linalg_setup','cusolverMgCreate failed',10)
#endif
    cuda_linalg_init=.true.
  end subroutine
  !
end module

