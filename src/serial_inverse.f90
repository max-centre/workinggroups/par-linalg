
!===========================================
  subroutine serial_inverse( ndim, Amat, Ainv )
  !===========================================
  use kinds
  use util_module
  use para_module
  implicit none
  !
  integer   :: ndim
  real(dbl) :: Amat(ndim,ndim)
  real(dbl) :: Ainv(ndim,ndim)
  integer   :: ierr
  !
  real(dbl), allocatable :: w(:), zmat(:,:)
  !
  call mat_inv( ndim, Amat, Ainv, IERR=ierr )
  if (ierr/=0 ) call errore('serial_inverse','inverting Amat',10)
  !
  ! compute and report eigenvalues of Ainv
  !
  allocate( w(ndim), zmat(ndim,ndim) )
  call mat_hdiag( zmat, w, Ainv, ndim )
  !
  if ( me_pool == root_pool ) then 
     write(*,"(5x,i4,' Ainv Eigenvalues')") my_pool_id
     !
     ! print the 8 largest eigs
     write(*,"(5x,i4,100f15.9)") my_pool_id, w(ndim:max(1,ndim-7):-1)
     ! print the 8 smallest  eigs
     write(*,"(5x,i4,100f15.9)") my_pool_id, w(1:min(ndim,8))
  endif
  !
  deallocate( w, zmat )
  !
#ifdef __MPI
  call MPI_barrier( mpi_comm_world, ierr)
#endif
  return
  !
end subroutine serial_inverse

