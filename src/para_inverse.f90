
!===========================================
  subroutine para_inverse( ndim, Amat, Ainv )
  !===========================================
  !
  ! perform the inversion by using scalapack
  !
  use kinds
  use util_module
  use para_module
  implicit none
  !
  integer, parameter :: dlen_ = 9
  !
  integer   :: ndim
  real(dbl) :: Amat(ndim,ndim)
  real(dbl) :: Ainv(ndim,ndim)
  !
  integer   :: info, ierr
#ifdef __SCALAPACK
  integer   :: descA(dlen_), descAinv(dlen_)
  !
  integer   :: nprow, npcol, myrow, mycol
  integer   :: ndim_blc, lld
  integer   :: lwork, liwork, itmp(2)
  integer   :: ils, ile, jls, jle
  real(dbl) :: rtmp(2)
  real(dbl), allocatable :: Amat_loc(:,:), Ainv_loc(:,:)
  real(dbl), allocatable :: buff(:,:)
  integer,   allocatable :: ipiv(:)
  integer,   allocatable :: iwork(:)
  real(dbl), allocatable :: work(:)
  real(dbl), allocatable :: w(:), zmat(:,:)

  !
  ! init global blacs grid
  !
  call BLACS_GRIDINFO( ortho_cntx, np_ortho(1), np_ortho(2), me_ortho(1), me_ortho(2) )
  !
  nprow=np_ortho(1)
  npcol=np_ortho(2)
  myrow=me_ortho(1)
  mycol=me_ortho(2)
  !
  ! spectator tasks
  if ( me_ortho(1) == -1 ) return   ! or do something else
  !
  !
  ! distribute the matrix on the process grid
  ! Initialize the array descriptors for the matrices A and B
  !
  ndim_blc = int(ndim/npcol)
  if (ndim_blc*npcol < ndim ) ndim_blc=ndim_blc+1
  !
  lld = ndim
  !
  call DESCINIT( descA, ndim, ndim, ndim_blc, ndim_blc, 0, 0, ortho_cntx, lld, info )
  !
  allocate( Amat_loc(ndim,ndim_blc) )
  allocate( Ainv_loc(ndim,ndim_blc) )
  allocate( ipiv(ndim+ndim_blc) )
  !
  ! LWORK  = LOCr(N+MOD(IA-1,MB_A))*NB_A
  ! LIWORK = LOCc( N_A + MOD(JA-1, NB_A) ) + NB_A

  !
  ! distribute the matrix A
  !
  ils=1 !myrow*ndim_blc+1
  ile=ndim !min(myrow*ndim_blc+ndim_blc,ndim)
  jls=mycol*ndim_blc+1
  jle=min((mycol+1)*ndim_blc,ndim)
  !
  Amat_loc=0.0d0
  Amat_loc=Amat(ils:ile,jls:jle)
  !
  ! perform the inversion
  !
  CALL PDGETRF( ndim, ndim, Amat_loc, 1, 1, descA, ipiv, info )
  if ( info /= 0 ) call errore('para_inverse','performing PDGETRF',info)
  !
  CALL PDGETRI( ndim, Amat_loc, 1, 1, descA, &
                ipiv, rtmp, -1, itmp, -1, info )
  if ( info /= 0 ) call errore('para_inverse','performing PDGETRI',10)
  !
  lwork  = int(rtmp(1))
  liwork = itmp(1)
  !
  allocate( work(lwork) )
  allocate( iwork(liwork) )
  !
  CALL PDGETRI( ndim, Amat_loc, 1, 1, descA, &
                ipiv, work, lwork, iwork, liwork, info )
  if ( info /= 0 ) call errore('para_inverse','performing PDGETRI',10)

  !
  ! gather the inverse matrix
  !
  Ainv=0.0d0
  Ainv(ils:ile,jls:jle)=Amat_loc(:,:)

  allocate( buff(ndim,ndim) )
  !
  buff = Ainv
  call MPI_ALLREDUCE( buff, Ainv, ndim*ndim, MPI_DOUBLE_PRECISION, MPI_SUM, intra_pool_comm, info)
  if ( info /= 0 ) call errore('para_inverse','performing MPIALLGATHER',10)
  !
  deallocate(buff)

  ! 
  ! local cleanup 
  ! 
  deallocate( Amat_loc, Ainv_loc )
  deallocate( ipiv, work, iwork )


  !
  ! compute and report eigenvalues of Ainv
  !
  allocate( w(ndim), zmat(ndim,ndim) )
  call mat_hdiag( zmat, w, Ainv, ndim )
  !
  if ( me_pool == root_pool ) then
     write(*,"(5x,i4,' Ainv Eigenvalues')") my_pool_id
     !   
     ! print the 8 largest eigs
     write(*,"(5x,i4,100f15.9)") my_pool_id, w(ndim:max(1,ndim-7):-1)
     ! print the 8 smallest  eigs
     write(*,"(5x,i4,100f15.9)") my_pool_id, w(1:min(ndim,8))
     call flush()
  endif
  !
  deallocate( w, zmat )
  !
#endif
  !
  call MPI_barrier( mpi_comm_world, ierr)
  return
  !
end subroutine para_inverse

