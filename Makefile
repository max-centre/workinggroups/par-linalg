

all:
	cd src ; make all

clean:
	cd src ; make clean

distclean: clean
	-rm make.sys
